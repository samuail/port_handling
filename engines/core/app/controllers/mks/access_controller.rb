require_dependency 'mks/application_controller'

module Mks
  class AccessController < ApplicationController
    before_action :confirm_logged_in, :except => [:attempt_login, :logout, :menu, :csrf_token]

    def csrf_token
      cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
      render json: {success: true}
    end

    def attempt_login
      app_module = ApplicationModule.find_by(code: params[:module_code])
      user = User.find_by(email: params[:email].downcase)

      if user && user.application_module.id == app_module.id
        if user.authenticate(params[:password])
          login_user user
          response = {success: true, data: user.full_name}
          render json: response
        else
          render json: {success: false, errors: "Invalid username or password"}
        end
      else
        render json: {success: false, errors: "User doesn't exist or is not allowed!"}
      end
    end

    def logout
      logout_user if logged_in?
      render json: MethodResponse.new(success: true)
    end

    def menu
      render json: {success: true, data: fetch_menus}
    end

    def check_login
      if session[:user_id]
        user = User.find(session[:user_id])
        render json: {success: true, data: user.full_name}
      else
        render json: {success: false}
      end
    end
  end
end
