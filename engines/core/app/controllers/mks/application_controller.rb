module Mks
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :null_session

    include AccessHelper

    def current_clearance_level
      current_user.roles.first.name
    end

    private

    def confirm_logged_in
      unless session[:user_id]
        redirect_to "/"
        false
      else
        true
      end
    end

    def token
      authenticate_with_http_basic do |email, password|
        user = User.find_by(email: email)
        if user && user.authenticate(password)
          render json: { success: true, data: user.full_name, token: user.auth_token }
        else
          render json: {success: false, errors: "Invalid username or password"}
        end
      end
    end

    protected

    def verified_request?
      super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
    end
  end
end
