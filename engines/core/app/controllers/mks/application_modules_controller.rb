require_dependency "mks/application_controller"

module Mks
  class ApplicationModulesController < ApplicationController
    before_action :set_application_module, only: [:show, :edit, :update, :destroy]

    # GET /application_modules
    def index
      @application_modules = ApplicationModule.all
    end

    # GET /application_modules/1
    def show
    end

    # GET /application_modules/new
    def new
      @application_module = ApplicationModule.new
    end

    # GET /application_modules/1/edit
    def edit
    end

    # POST /application_modules
    def create
      @application_module = ApplicationModule.new(application_module_params)

      if @application_module.save
        redirect_to @application_module, notice: 'Application module was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /application_modules/1
    def update
      if @application_module.update(application_module_params)
        redirect_to @application_module, notice: 'Application module was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /application_modules/1
    def destroy
      @application_module.destroy
      redirect_to application_modules_url, notice: 'Application module was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_application_module
        @application_module = ApplicationModule.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def application_module_params
        params.require(:application_module).permit(:code, :name)
      end
  end
end
