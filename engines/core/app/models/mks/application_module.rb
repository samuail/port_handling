module Mks
  class ApplicationModule < ActiveRecord::Base
    has_many :users
    has_many :menus
  end
end
