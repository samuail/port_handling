module Mks
  class Menu < ActiveRecord::Base
    belongs_to :application_module
    belongs_to :parent, :class_name => 'Menu'
    has_many :children, :class_name => 'Menu', :foreign_key => 'parent_id'
    has_and_belongs_to_many :user_roles, :join_table => :mks_menus_user_roles
  end
end
