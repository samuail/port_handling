module Mks
  class User < ActiveRecord::Base
    belongs_to :application_module
    has_and_belongs_to_many :roles, :class_name => UserRole, :join_table => :mks_users_user_roles
    has_secure_password

    before_save { email.downcase! }

    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :first_name, presence: true, length: { maximum: 30 }
    validates :last_name, presence: true, length: { maximum: 30 }
    validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
    validates :password, length: { minimum: 6 }

    def full_name
      "#{first_name} #{last_name}"
    end
  end
end
