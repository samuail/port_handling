module Mks
  class UserRole < ActiveRecord::Base
    has_and_belongs_to_many :users, :join_table => :mks_users_user_roles
    has_and_belongs_to_many :menus, :join_table => :mks_menus_user_roles
  end
end
