Mks::Core::Engine.routes.draw do

  get '/csrf_token', to: 'access#csrf_token'

  get '/attempt_login', to: 'access#attempt_login'

  get '/logout', to: 'access#logout'

  get '/menu', to: 'access#menu'

  get '/check_login', to: 'access#check_login'

  post '/login', to: 'access#attempt_login'

  resources :application_modules
end
