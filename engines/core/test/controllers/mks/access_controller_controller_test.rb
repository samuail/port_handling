require 'test_helper'

module Mks
  class AccessControllerControllerTest < ActionController::TestCase
    setup do
      @routes = Engine.routes
    end

    test "should get csrf_token" do
      get :csrf_token
      assert_response :success
    end

    test "should get attempt_login" do
      get :attempt_login
      assert_response :success
    end

    test "should get logout" do
      get :logout
      assert_response :success
    end

    test "should get menu" do
      get :menu
      assert_response :success
    end

    test "should get check_login" do
      get :check_login
      assert_response :success
    end

  end
end
