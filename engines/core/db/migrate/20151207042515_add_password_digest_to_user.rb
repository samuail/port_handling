class AddPasswordDigestToUser < ActiveRecord::Migration
  def up
    add_column "mks_users", "password_digest", :string
  end

  def down
    remove_column "mks_users", "password_digest"
  end
end
