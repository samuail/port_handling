class CreateMksUserRoles < ActiveRecord::Migration
  def change
    create_table :mks_user_roles do |t|
      t.string :name
      t.timestamps null: false
    end

    create_table(:mks_users_user_roles, :id => false) do |t|
      t.references :user
      t.references :user_role
    end

    add_index(:mks_users_user_roles, [ :user_id, :user_role_id ])
  end
end
