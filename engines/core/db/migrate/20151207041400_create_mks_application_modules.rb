class CreateMksApplicationModules < ActiveRecord::Migration
  def change
    create_table :mks_application_modules do |t|
      t.string :code
      t.string :name

      t.timestamps null: false
    end
  end
end
