class CreateMksMenus < ActiveRecord::Migration
  def change
    create_table :mks_menus do |t|
      t.string :text
      t.string :icon_cls
      t.string :class_name
      t.integer :parent_id
      t.string :location
      t.integer :application_module_id

      t.timestamps null: false
    end

    create_table(:mks_menus_user_roles, :id => false) do |t|
      t.references :menu
      t.references :user_role
    end

    add_index(:mks_menus_user_roles, [ :menu_id, :user_role_id ])
  end
end
