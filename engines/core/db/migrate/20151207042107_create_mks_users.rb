class CreateMksUsers < ActiveRecord::Migration
  def change
    create_table :mks_users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.boolean :active
      t.integer :application_module_id

      t.timestamps null: false
    end
  end
end
