$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "mks/core/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'mks_core'
  s.version     = Mks::Core::VERSION
  s.authors     = ['Henock L.']
  s.email       = ['henockl@live.com']
  s.homepage    = 'http://www.mks.com'
  s.summary     = 'Core features of MKS'
  s.description = 'Core feature of MKS which will be used with other MKS applications'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.rdoc']

  s.test_files = Dir['']

  s.add_dependency 'rails', '~> 4.2.4'

  s.add_dependency 'bcrypt'

  s.add_development_dependency 'rspec-rails'

  s.add_development_dependency 'cucumber-rails'

  s.add_development_dependency "sqlite3"
end
