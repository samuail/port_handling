class ChargeableServiceIncrement < ActiveRecord::Base
  belongs_to :chargeable_service_unit_of_charge
  belongs_to :service_increment
end
