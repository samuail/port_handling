class ServiceRate < ActiveRecord::Base
  belongs_to :chargeable_service_unit_of_charge
  belongs_to :container_size
  belongs_to :transaction_type
  has_many :service_prices

  def self.get_service_delivery_units
    transport_id = ServiceDeliveryUnitType.select(:id).where('lower(name) = ?', 'Transport'.downcase)
    return ServiceDeliveryUnit.where.not('service_delivery_unit_type_id' => transport_id)
  end

  def self.get_transaction_types
    return TransactionType.all
  end

  def self.get_container_sizes
    return ContainerSize.all
  end

  def self.create_archive
    self.connection.execute("select create_service_rate_archive()")
  end

  def self.get_chargeable_services(service_delivery_unit_id)
    unit_of_charge_id = UnitOfCharge.select(:id).where('code' => 'CNT')
    acsuocs = ChargeableServiceUnitOfCharge.where('service_delivery_unit_id' => service_delivery_unit_id, 'unit_of_charge_id' => unit_of_charge_id )
    return acsuocs
  end

  def self.generate_rate_for_containerized_chargeable_service(effective_date)
    service_delivery_units = get_service_delivery_units
    transaction_types = get_transaction_types
    container_sizes = get_container_sizes
    service_delivery_units.each{ |sdu|
      transaction_types.each { |transaction_type|
        container_sizes.each {|container_size|
          chargeable_services = get_chargeable_services(sdu.id)
          chargeable_services.each { |cs|
            service_rate = ServiceRate.where('chargeable_service_unit_of_charge_id' => cs.id, 'transaction_type_id' => transaction_type.id,
                                             'container_size_id' => container_size.id)
            if service_rate.count == 0
              ServiceRate.create('chargeable_service_unit_of_charge_id' => cs.id, 'transaction_type_id' => transaction_type.id,
                                 'container_size_id' => container_size.id, 'effective_date' => effective_date, 'low' => 0, 'medium' => 0, 'high' => 0)
            end
          }
        }
      }
    }
  end
end
