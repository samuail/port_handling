class ServiceIncrement < ActiveRecord::Base
  validates :code, presence: true
  has_many :increment_rates
  has_many :addendums
  has_many :additions, :through => :addendums
end
