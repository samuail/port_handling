class ServiceDeliveryUnit < ActiveRecord::Base
  validates :code, :presence => true
  belongs_to :currency
  belongs_to :service_delivery_unit_type
  has_many :service_delivery_unit_services
  has_many :chargeable_services, :through => :service_delivery_unit_services
end
