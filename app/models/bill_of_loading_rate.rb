class BillOfLoadingRate < ActiveRecord::Base
  belongs_to :chargeable_service_unit_of_charge
  belongs_to :transaction_type

  def self.get_chargeable_services(service_delivery_unit_id)
    unit_of_charge_id = UnitOfCharge.select(:id).where('code' => 'BL')
    csuocs = ChargeableServiceUnitOfCharge.where('service_delivery_unit_id' => service_delivery_unit_id, 'unit_of_charge_id' => unit_of_charge_id )
    return csuocs
  end

  def self.get_transaction_types
    return TransactionType.all
  end

  def self.get_delivery_units
    return ServiceDeliveryUnit.all
  end

  def self.create_archive
    self.connection.execute("select create_bill_of_loading_rate_archive()")
  end

  def self.generate_rate_for_bill_of_loading_services(effective_date)
    transaction_types = get_transaction_types
    service_delivery_units = get_delivery_units
    service_delivery_units.each { |sdu|
      transaction_types.each { |transaction_type|
        chargeable_services = get_chargeable_services(sdu.id)
        chargeable_services.each { |chargeable_service|
          bill_of_loading_rate = BillOfLoadingRate.where('transaction_type_id' =>transaction_type.id,
                                                         'chargeable_service_unit_of_charge_id' => chargeable_service.id)
          if bill_of_loading_rate.count == 0
            BillOfLoadingRate.create('transaction_type_id' => transaction_type.id,'chargeable_service_unit_of_charge_id' =>
                                      chargeable_service.id, 'low' => 0, 'medium' => 0,'high' => 0,'effective_date' => effective_date)
          end
        }
      }
    }
  end
end
