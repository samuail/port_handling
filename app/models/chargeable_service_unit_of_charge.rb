class ChargeableServiceUnitOfCharge < ActiveRecord::Base
  belongs_to :service_delivery_unit
  belongs_to :chargeable_service
  belongs_to :unit_of_charge
  has_many :service_rates
  has_many :bill_of_loading_rates
  has_many :break_bulk_rates
  has_many :truck_rates
  has_many :chargeable_service_increments
  has_many :service_increments, :through => :chargeable_service_increments

  def self.get_service_delivery_units
    return ServiceDeliveryUnit.all
  end

  def self.get_chargeable_service_for_sdu(sdu_id)
    cs_ids = ServiceDeliveryUnitService.select(:chargeable_service_id).where('service_delivery_unit_id' => sdu_id)
    return ChargeableService.where('id' =>cs_ids)
  end

  def self.generate_chargeable_service_units
    service_delivery_units = get_service_delivery_units
    service_delivery_units.each { |sdu|
      chargeable_services = get_chargeable_service_for_sdu(sdu.id)
      chargeable_services.each { |cs|
        if ChargeableServiceUnitOfCharge.where('service_delivery_unit_id' => sdu.id, 'chargeable_service_id' =>cs.id).count == 0
          ChargeableServiceUnitOfCharge.create('service_delivery_unit_id' => sdu.id, 'chargeable_service_id' =>cs.id, 'has_increment' =>FALSE)
        end
      }
    }
  end
end
