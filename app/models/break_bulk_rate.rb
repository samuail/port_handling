class BreakBulkRate < ActiveRecord::Base
  belongs_to :chargeable_service_unit_of_charge
  belongs_to :transaction_type

  def self.get_service_delivery_units
    return ServiceDeliveryUnit.all
  end

  def self.get_transaction_types
    return TransactionType.all
  end

  def self.get_chargeable_services_for_service_delivery_unit(service_delivery_unit_id)
    unit_of_charge_id = UnitOfCharge.select(:id).where('code' => 'FT')
    csuocs = ChargeableServiceUnitOfCharge.where('service_delivery_unit_id' => service_delivery_unit_id, 'unit_of_charge_id'=>unit_of_charge_id)
    return csuocs
  end

  def self.create_archive
    self.connection.execute("select create_break_bulk_rate_archive()")
  end

  def self.generate_rate_for_break_bulk(effective_date)
    service_delivery_units = get_service_delivery_units
    transaction_types = get_transaction_types
    service_delivery_units.each { |sdu|
      transaction_types.each { |tt|
        chargeable_services = get_chargeable_services_for_service_delivery_unit(sdu.id)
        chargeable_services.each { |cs|
          service_delivery_unit_rate = BreakBulkRate.where('chargeable_service_unit_of_charge_id' => cs.id, 'transaction_type_id' => tt.id)
          if service_delivery_unit_rate.count == 0
            BreakBulkRate.create('chargeable_service_unit_of_charge_id' => cs.id,
                                 'transaction_type_id' => tt.id, 'low' => 0, 'medium' => 0, 'high' => 0 , 'effective_date' => effective_date)
          end
        }
      }
    }
  end
end
