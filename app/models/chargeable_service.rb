class ChargeableService < ActiveRecord::Base
  validates :code, presence: true
  validates :name, :presence => true
  has_many :service_rates
  has_many :chargeable_service_increments
  has_many :chargeable_service_unit_of_charges, :through => :chargeable_service_increments
end
