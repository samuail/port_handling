class ServiceDeliveryUnitService < ActiveRecord::Base
  validates :chargeable_service_id, :presence => true
  belongs_to :chargeable_service
end
