class Currency < ActiveRecord::Base
  validates :code, :name, presence: true, uniqueness: true
  has_many :rates

  def current_rate
    if self.rates[0].nil?
      nil
    else
      self.rates[0].rate_to_base / 100
    end
  end

  def self.convert_currency(suggested_currency, default_currency, unit_price)
    result = []
    if not suggested_currency.nil? and not suggested_currency == ''
      if suggested_currency != default_currency
        old_rate = Currency.find(default_currency).current_rate
        new_rate = Currency.find(suggested_currency).current_rate
        return result.push(unit_price / old_rate * new_rate, suggested_currency)
      end
    end
    return result.push(unit_price, default_currency)
  end
end
