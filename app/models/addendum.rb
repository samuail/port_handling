class Addendum < ActiveRecord::Base
  belongs_to :service_increment
  belongs_to :addition, :class_name => 'ServiceIncrement'
end
