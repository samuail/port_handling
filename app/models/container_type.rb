class ContainerType < ActiveRecord::Base
  validates :code, :name, presence: true, uniqueness: true
end
