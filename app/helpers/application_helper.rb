module ApplicationHelper
  def confirm_logged_in
    unless session[:user_id]
      redirect_to(:controller => 'access', :action => 'login')
      return false
    else
      return true
    end
  end
end
