class ServiceIncrementsController < ApplicationController
  before_action :set_service_increment, only: [:update]

  # GET /service_increments
  # GET /service_increments.json
  def index
    @service_increments = ServiceIncrement.all
    @response = {:success => true, :message => '', :data => @service_increments}
    render  json: @response
  end

  # POST /service_increments
  # POST /service_increments.json
  def create
    @service_increment = ServiceIncrement.new(service_increment_params)

    #respond_to do |format|
    #format.json do
    if @service_increment.valid?
      @service_increment.save
      @response = {:success => true, :message => 'Increment recorded successfully', :data => @service_increment}
      render json: @response
    else
      @response = {:success => false, :message => 'Error saving increment', :data => []}
      render json: @response
    end
    #end
    #end
  end

  # PATCH/PUT /service_increments/1
  # PATCH/PUT /service_increments/1.json
  def update
    @service_increment.update_attributes(service_increment_params)
    if @service_increment.save
      @response = {:success => true, :message => 'Increment updated successfully', :data => @service_increment}
      render json: @response
    end
  end

  def increments
    service_increment_id = params[:id]
    service_increment = ServiceIncrement.find(service_increment_id)
    @response = {:success => true, :message => '', :data => service_increment.additions}
    render json: @response
  end

  def unassigned_increments
    service_increment_id = params[:id]
    assigned_increments = Addendum.select(:service_increment_id).where('service_increment_id' => service_increment_id)
    assigned_increment_ids = [service_increment_id]
    if assigned_increments.count > 0
      assigned_increment_ids.push(assigned_increments)
    end
    unassigned_increments = ServiceIncrement.where.not(id: assigned_increment_ids)
    @response ={:success =>true, :message => '', :data => unassigned_increments}
    render json: @response
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_increment
      @service_increment = ServiceIncrement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_increment_params
      params.require(:service_increment).permit(:code, :name, :has_increment)
    end
end
