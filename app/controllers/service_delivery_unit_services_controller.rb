class ServiceDeliveryUnitServicesController < ApplicationController

  # POST /service_delivery_unit_services
  # POST /service_delivery_unit_services.json
  def create
    @service_delivery_unit_service = ServiceDeliveryUnitService.new(service_delivery_unit_service_params)

    if @service_delivery_unit_service.valid?
      @service_delivery_unit_service.save
      @response = {:success => true, :message => 'Service delivery unit service(s) recorded successfully', :data => @service_delivery_unit}
      render json: @response
    else
      @response = {:success => false, :message => 'Error saving service delivery unit service(s)', :data => []}
      render json: @response
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_delivery_unit_service
      @service_delivery_unit_service = ServiceDeliveryUnitService.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_delivery_unit_service_params
      params.require(:service_delivery_unit_service).permit(:service_delivery_unit_id, :chargeable_service_id)
    end
end
