class AddendumsController < ApplicationController

  # GET /addendums
  # GET /addendums.json
  def index
    @addendums = Addendum.where('service_increment_id' => params[:service_increment_id])
    @response = {:success => true, :message => '', :data => @addendums}
    render json: @response
  end

  # POST /addendums
  # POST /addendums.json
  def create
    @addendum = Addendum.new(addendum_params)

    if @addendum.valid?
      service_increment = ServiceIncrement.find(params[:service_increment_id])
      addendum = service_increment.addendums << @addendum
      @response = {:success => true, :message => 'Addition created successfully', :data => addendum}
      render json: @response
    else
      @response = {:success => false, :message => 'Error saving addition', :data => []}
      render json: @response
    end
  end

  def get_all
    @addendums = Addendum.all
    @response = {:success => true, :message =>'', :data =>@addendums}
    render json: @response
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_addendum
      @addendum = Addendum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def addendum_params
      params.require(:addendum).permit(:service_increment_id, :addition_id)
    end
end
