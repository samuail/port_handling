class IncrementRatesController < ApplicationController
  before_action :set_increment_rate, only: [:update]

  # GET /increment_rates
  # GET /increment_rates.json
  def index
    service_increment_id = params[:service_increment_id]
    service_increment = ServiceIncrement.find(service_increment_id)
    @increment_rates = service_increment.increment_rates
    @response = {:success => true, :message => '', :data => @increment_rates}
    render json: @response
  end


  # POST /increment_rates
  # POST /increment_rates.json
  def create
    @increment_rate = IncrementRate.new(increment_rate_params)
    if @increment_rate.valid?
      service_increment_id = params[:service_increment_id]
      service_increment = ServiceIncrement.find(service_increment_id)
      service_increment.increment_rates << @increment_rate
      @response = {:success => true, :message => 'Increment rate recorded successfully', :data => @increment_rate}
      render json: @response
    else
      @response = {:success => false, :message => 'Error saving increment rate', :data => []}
      render json: @response
    end
  end

  # PATCH/PUT /increment_rates/1
  # PATCH/PUT /increment_rates/1.json
  def update
    service_increment = ServiceIncrement.find(params[:service_increment_id])
    @increment_rate = service_increment.increment_rates.find(params[:id])
    @increment_rate.update_attributes(increment_rate_params)
    if @increment_rate.save
      @response = {:success => true, :message => 'Increment rate updated successfully', :data => @increment_rate}
      render json: @response
    end
  end

  def get_all
    @increment_rates = IncrementRate.all
    @response = {:success => true, :message => '', :data =>@increment_rates}
    render json: @response
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_increment_rate
      @increment_rate = IncrementRate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def increment_rate_params
      params.require(:increment_rate).permit(:percent, :effective_date, :service_increment_id)
    end
end
