class ServiceRatesController < ApplicationController
  before_action :set_service_rate, only: [:update]

  # GET /service_rates
  # GET /service_rates.json
  def index
    service_rate_array = []
    @service_rates = ServiceRate.all#includes('chargeable_service').order("chargeable_services.order")
    @service_rates.each { |service_rate|
      service_rate_array.push({:id => service_rate.id,
                              :chargeable_service_name => service_rate.chargeable_service_unit_of_charge.chargeable_service.name,
                              :effective_date => service_rate.effective_date,:low => service_rate.low,
                              :medium => service_rate.medium, :high => service_rate.high,
                              :container_size_id => service_rate.container_size_id,
                              :container_size_name => service_rate.container_size.name,
                              :transaction_type_id => service_rate.transaction_type_id,
                              :transaction_type_name => service_rate.transaction_type.name,
                              :service_delivery_unit_id => service_rate.chargeable_service_unit_of_charge.service_delivery_unit_id,
                              :service_delivery_unit_name => service_rate.chargeable_service_unit_of_charge.service_delivery_unit.name,
                              })
    }
    @response = {:success => true, :message => '', :data => service_rate_array}
    render json: @response
  end

  # POST /service_rates
  # POST /service_rates.json
  def create
    ServiceRate.generate_rate_for_containerized_chargeable_service(params[:effective_date])
    @response = {:success => true, :message => 'Rate table generated successfully', :data => []}
    render json: @response
  end

  # PATCH/PUT /service_rates/1
  # PATCH/PUT /service_rates/1.json
  def update
    maxLowRate = ServiceRate.maximum('low')
    maxMediumRate = ServiceRate.maximum('medium')
    maxHighRate = ServiceRate.maximum('high')
    maximumRate = [maxLowRate,maxMediumRate,maxHighRate].max
    if maximumRate > 0
      ServiceRate.create_archive
    end
    service_rates = params[:updated_service_rates]
    service_rates.each { |service_rate|
      old_service_rate = ServiceRate.find(service_rate['id'])
      old_service_rate.low = service_rate['low']
      old_service_rate.medium = service_rate['medium']
      old_service_rate.high = service_rate['high']
      old_service_rate.effective_date = service_rate['effective_date']
      old_service_rate.save
    }
    @response = {:success => true, :message => 'Rate table updated successfully', :data => service_rates}
    render json: @response
  end


  def get_chargeable_services_for_service_delivery_unit
    service_delivery_unit_id = params[:service_delivery_unit_id]
    chargeable_service_ids = ServiceRate.select(:chargeable_service_id).where('service_delivery_unit_id' => service_delivery_unit_id)
    chargeable_services = ChargeableService.where(id: chargeable_service_ids)
    @response = {:success => true, :message => '', :data => chargeable_services}
    render json: @response
  end

  def get_rate_for_chargeable_service
    chargeable_service_id = params[:chargeable_service_id]
    service_delivery_unit_id = params[:id]
    chargeable_service_rate_array = []
    chargeable_service_rates = ServiceRate.where('chargeable_service_id' => chargeable_service_id, 'service_delivery_unit_id' => service_delivery_unit_id)
    chargeable_service_rates.each{ |csr|
      chargeable_service_rate_array.push({:id => csr.id,
           :chargeable_service_name => csr.chargeable_service.name,
           :effective_date => csr.effective_date,:low => csr.low,
           :medium => csr.medium, :high => csr.high,
           :container_size_id => csr.container_size_id,
           :container_size_name => csr.container_size.name,
           :transaction_type_id => csr.transaction_type_id,
           :transaction_type_name => csr.transaction_type.name,
           :service_delivery_unit_id => csr.service_delivery_unit_id,
           :service_delivery_unit_name => csr.service_delivery_unit.name})
    }
    @response = {:success => true, :message => '', :data => chargeable_service_rate_array}
    render json: @response
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_rate
      @service_rate = ServiceRate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_rate_params
      params.require(:service_rate).permit(:container_size_id, :transaction_type_id, :service_delivery_unit_id, :chargeable_service_id, :unit_price)
    end
end
