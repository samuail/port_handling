class ServiceDeliveryUnitTypesController < ApplicationController
  before_action :set_service_delivery_unit_type, only: [:update]

  # GET /service_delivery_unit_types
  # GET /service_delivery_unit_types.json
  def index
    @service_delivery_unit_types = ServiceDeliveryUnitType.all
    @response = {:success => true, :message => '', :data => @service_delivery_unit_types}
    render json: @response
  end

  # POST /service_delivery_unit_types
  # POST /service_delivery_unit_types.json
  def create
    @service_delivery_unit_type = ServiceDeliveryUnitType.new(service_delivery_unit_type_params)

    respond_to do |format|
      if @service_delivery_unit_type.save
        format.html { redirect_to @service_delivery_unit_type, notice: 'Service delivery unit type was successfully created.' }
        format.json { render :show, status: :created, location: @service_delivery_unit_type }
      else
        format.html { render :new }
        format.json { render json: @service_delivery_unit_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /service_delivery_unit_types/1
  # PATCH/PUT /service_delivery_unit_types/1.json
  def update
    respond_to do |format|
      if @service_delivery_unit_type.update(service_delivery_unit_type_params)
        format.html { redirect_to @service_delivery_unit_type, notice: 'Service delivery unit type was successfully updated.' }
        format.json { render :show, status: :ok, location: @service_delivery_unit_type }
      else
        format.html { render :edit }
        format.json { render json: @service_delivery_unit_type.errors, status: :unprocessable_entity }
      end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_delivery_unit_type
      @service_delivery_unit_type = ServiceDeliveryUnitType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_delivery_unit_type_params
      params.require(:service_delivery_unit_type).permit(:code, :name)
    end
end
