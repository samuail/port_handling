class BreakBulkUnitRatesController < ApplicationController
  before_action :set_break_bulk_unit_rate, only: [:update]

  # GET /break_bulk_unit_rates
  # GET /break_bulk_unit_rates.json
  def index
    @break_bulk_unit_rates = BreakBulkUnitRate.all
    break_bulk_unit_rate_array = []
    @break_bulk_unit_rates.each { |bbur|
      break_bulk_unit_rate_array.push({:id => bbur.id,
                                      :service_delivery_unit_name => bbur.chargeable_service_unit_of_charge.service_delivery_unit.name,
                                      :chargeable_service_name => bbur.chargeable_service_unit_of_charge.chargeable_service.name,
                                      :transaction_type_name => bbur.transaction_type.name,
                                      :break_bulk_unit_name => bbur.break_bulk_unit.name,
                                      :low => bbur.low, :medium => bbur.medium, :high => bbur.high,
                                      :effective_date => bbur.effective_date})
    }
    response = MethodResponse.new(true, nil, break_bulk_unit_rate_array, nil, nil)
    render json: response
  end

  # POST /break_bulk_unit_rates
  # POST /break_bulk_unit_rates.json
  def create
    BreakBulkUnitRate.generate_rate_for_break_bulk_units(params[:effective_date])
    response = MethodResponse.new(true, 'Break bulk unit rate generated successfully !', nil, nil, nil)
    render json: response
  end

  # PATCH/PUT /break_bulk_unit_rates/1
  # PATCH/PUT /break_bulk_unit_rates/1.json
  def update
    maxLowRate = BreakBulkUnitRate.maximum('low')
    maxMediumRate = BreakBulkUnitRate.maximum('medium')
    maxHighRate = BreakBulkUnitRate.maximum('high')
    maximumRate = [maxLowRate,maxMediumRate,maxHighRate].max

    if maximumRate > 0
      #BreakBulkUnitRate.create_archive
    end
    service_rates = params[:updated_service_rates]
    service_rates.each { |service_rate|
      old_service_rate = BreakBulkUnitRate.find(service_rate['id'])
      old_service_rate.low = service_rate['low']
      old_service_rate.medium = service_rate['medium']
      old_service_rate.high = service_rate['high']
      old_service_rate.effective_date = service_rate['effective_date']
      old_service_rate.save
    }
    @response = {:success => true, :message => 'Rate table updated successfully', :data => service_rates}
    render json: @response
  end


  private

    def set_break_bulk_unit_rate
      @break_bulk_unit_rate = BreakBulkUnitRate.find(params[:id])
    end

    def break_bulk_unit_rate_params
      params.require(:break_bulk_unit_rate).permit(:chargeable_service_unit_of_charge_id, :transaction_type_id, :integer, :break_bulk_unit_id, :high, :medium, :low, :effective_date)
    end
end
