class BreakBulkUnitsController < ApplicationController
  before_action :confirm_logged_in
  before_action :set_break_bulk_unit, only: [:update]

  # GET /break_bulk_units
  # GET /break_bulk_units.json
  def index
    @break_bulk_units = BreakBulkUnit.all
    response = MethodResponse.new(true,nil,@break_bulk_units,nil,nil)
    render json: response
  end

  # POST /break_bulk_units
  # POST /break_bulk_units.json
  def create
    @break_bulk_unit = BreakBulkUnit.new(break_bulk_unit_params)

    if @break_bulk_unit.valid?
      @break_bulk_unit.save
      @response = MethodResponse.new(true,'Break bulk unit recorded successfully',nil,nil,nil)
      render json: @response
    else
      errors = Util.error_messages @break_bulk_unit, 'Break Bulk Unit'
      @response = MethodResponse.new(false,nil,nil,errors,nil)
      render json: @response
    end
  end

  # PATCH/PUT /break_bulk_units/1
  # PATCH/PUT /break_bulk_units/1.json
  def update
    @break_bulk_unit.update_attributes(break_bulk_unit_params)
    if @break_bulk_unit.save
      @response = MethodResponse.new(true,'Break bulk unit updated successfully',nil,nil,nil)
      render json: @response
    end
  end


  private

    def set_break_bulk_unit
      @break_bulk_unit = BreakBulkUnit.find(params[:id])
    end

    def break_bulk_unit_params
      params.require(:break_bulk_unit).permit(:code, :name)
    end
end
