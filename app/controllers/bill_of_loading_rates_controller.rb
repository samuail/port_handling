class BillOfLoadingRatesController < ApplicationController
  before_action :set_bill_of_loading_rate, only: [:update]

  # GET /bill_of_loading_rates
  # GET /bill_of_loading_rates.json
  def index
    @bill_of_loading_rates = BillOfLoadingRate.all
    bill_of_loading_rates_array =[]
    @bill_of_loading_rates.each { |bolr|
      bill_of_loading_rates_array.push({:id => bolr.id, :high => bolr.high, :low => bolr.low, :medium => bolr.medium,
                                       :effective_date => bolr.effective_date,
                                       :service_delivery_unit_name => bolr.chargeable_service_unit_of_charge.service_delivery_unit.name,
                                       :service_delivery_unit_id => bolr.chargeable_service_unit_of_charge.service_delivery_unit.id,
                                       :transaction_type_id => bolr.transaction_type_id, :transaction_type_name =>
                                       bolr.transaction_type.name,
                                       :chargeable_service_id => bolr.chargeable_service_unit_of_charge.chargeable_service_id,
                                       :chargeable_service_name => bolr.chargeable_service_unit_of_charge.chargeable_service.name})
    }
    @response = {:success => true, :message => '', :data => bill_of_loading_rates_array}
    render json: @response
  end

  # POST /bill_of_loading_rates
  # POST /bill_of_loading_rates.json
  def create
    effective_date = params[:effective_date]
    BillOfLoadingRate.generate_rate_for_bill_of_loading_services(effective_date)
    @response = {:success => true, :message => 'Rate table generated successfully', :data => []}
    render json: @response
  end

  # PATCH/PUT /bill_of_loading_rates/1
  # PATCH/PUT /bill_of_loading_rates/1.json
  def update
    maxLowRate = BillOfLoadingRate.maximum('low')
    maxMediumRate = BillOfLoadingRate.maximum('medium')
    maxHighRate = BillOfLoadingRate.maximum('high')
    maximumRate = [maxLowRate,maxMediumRate,maxHighRate].max

    if maximumRate > 0
      BillOfLoadingRate.create_archive
    end
    service_rates = params[:updated_service_rates]
    service_rates.each { |service_rate|
      old_service_rate = BillOfLoadingRate.find(service_rate['id'])
      old_service_rate.low = service_rate['low']
      old_service_rate.medium = service_rate['medium']
      old_service_rate.high = service_rate['high']
      old_service_rate.effective_date = service_rate['effective_date']
      old_service_rate.save
    }
    @response = {:success => true, :message => 'Rate table updated successfully', :data => service_rates}
    render json: @response
  end

  def get_chargeable_service_for_service_delivery_unit
    service_delivery_unit_id = params[:service_delivery_unit_id]
    chargeable_services_array =[]
    chargeable_service_ids = BillOfLoadingRate.select(:'chargeable_service_id').where('service_delivery_unit_id' =>service_delivery_unit_id)
    chargeable_services = ChargeableService.where(id: chargeable_service_ids)
    chargeable_services.each { |cs|
      chargeable_services_array.push({:id =>cs.id, :name => cs.name, :code => cs.code})
    }
    @response ={:success => true, :message => '', :data => chargeable_services_array}
    render json: @response
  end

  def get_rate_for_chargeable_service
    service_delivery_unit_id = params[:id]
    chargeable_service_id = params[:chargeable_service_id]
    chargeable_service_rates = BillOfLoadingRate.where('service_delivery_unit_id' => service_delivery_unit_id,
                                                   'chargeable_service_id' => chargeable_service_id)
    chargeable_service_rates_array = []
    chargeable_service_rates.each { |csr|
      chargeable_service_rates_array.push({:id =>csr.id, :service_delivery_unit_id => csr.service_delivery_unit_id,
                                           :service_delivery_unit_name => csr.service_delivery_unit.name,
                                           :transaction_type_id => csr.transaction_type_id,:transaction_type_name => csr.transaction_type.name,
                                           :chargeable_service_id => csr.chargeable_service_id, :chargeable_service_name => csr.chargeable_service.name,
                                           :rate => csr.rate, :effective_date => csr.effective_date})
    }
    @response ={:success => true, :message => '', :data =>chargeable_service_rates_array}
    render json: @response
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bill_of_loading_rate
      @bill_of_loading_rate = BillOfLoadingRate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bill_of_loading_rate_params
      params.require(:bill_of_loading_rate).permit(:chargeable_service_id, :transaction_type_id, :service_delivery_unit_id, :rate, :effective_date)
    end
end
