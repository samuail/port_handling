class ChargeableServiceUnitOfChargesController < ApplicationController
  # GET /chargeable_service_unit_of_charges
  # GET /chargeable_service_unit_of_charges.json
  def index
    @chargeable_service_unit_of_charges = ChargeableServiceUnitOfCharge.all
    chargeable_service_units_array = []
    @chargeable_service_unit_of_charges.each { |csuoc|
      unit_of_charge_name = nil
      unit_of_charge_id = nil
      if csuoc.unit_of_charge_id != nil
        unit_of_charge_id = csuoc.unit_of_charge_id
        unit_of_charge_name = csuoc.unit_of_charge.name
      end
      chargeable_service_units_array.push({:id => csuoc.id, :service_delivery_unit_id => csuoc.service_delivery_unit_id,
                                          :service_delivery_unit_name => csuoc.service_delivery_unit.name,
                                          :chargeable_service_id => csuoc.chargeable_service_id,
                                          :chargeable_service_name => csuoc.chargeable_service.name,
                                          :unit_of_charge_id => unit_of_charge_id,
                                          :unit_of_charge_name => unit_of_charge_name,
                                          :has_increment => csuoc.has_increment})
    }
    response = MethodResponse.new(true,nil,chargeable_service_units_array,nil,nil)
    render json: response
  end

  # POST /chargeable_service_unit_of_charges
  # POST /chargeable_service_unit_of_charges.json
  def create
    ChargeableServiceUnitOfCharge.generate_chargeable_service_units
    response = MethodResponse.new(true, 'Chargeable services and unit of charges associated successfully!',nil,nil,nil)
    render json: response
  end

  def update_changes
    if not params[:updated_unit_of_charges].nil?
      updated_unit_of_charges = params[:updated_unit_of_charges]

      updated_unit_of_charges.each { |unit_of_charge|
        if unit_of_charge['unit_of_charge_id'].nil?
          old_cs_uoc = ChargeableServiceUnitOfCharge.find(unit_of_charge['id'])
          old_cs_uoc.has_increment = unit_of_charge['has_increment']
          old_cs_uoc.save
        else
          old_cs_uoc = ChargeableServiceUnitOfCharge.find(unit_of_charge['id'])
          old_cs_uoc.unit_of_charge_id = unit_of_charge['unit_of_charge_id']
          old_cs_uoc.has_increment = unit_of_charge['has_increment']
          old_cs_uoc.save
        end
      }
    end
    response = MethodResponse.new(true, 'Chargeable services unit of charges updated successfully!')
    render json: response
  end

  def increments
    @cs_service_unit_of_charge = ChargeableServiceUnitOfCharge.find(params[:id])
    service_increments = @cs_service_unit_of_charge.service_increments
    response = MethodResponse.new(true,nil,service_increments,nil,nil)
    render json: response
  end

  def unassigned_increments
    assigned_increments = ChargeableServiceIncrement.select('service_increment_id').where('chargeable_service_unit_of_charge_id'=>params[:id])
    unassigned_increments = ServiceIncrement.where.not(id: assigned_increments)
    response = MethodResponse.new(true,nil,unassigned_increments,nil,nil)
    render json: response
  end


  private

    def chargeable_service_unit_of_charge_params
      params.require(:chargeable_service_unit_of_charge).permit(:service_delivery_unit_id, :chargeable_service_id, :unit_of_charge_id, :has_increment)
    end
end
