class ContainerSizesController < ApplicationController
  before_action :set_container_size, only: [:show, :edit, :update, :destroy]

  # GET /container_sizes
  # GET /container_sizes.json
  def index
    @container_sizes = ContainerSize.all
    @response = {:success => true, :message => '', :data => @container_sizes}
    render json: @response
  end

  # GET /container_sizes/1
  # GET /container_sizes/1.json
  def show
  end

  # GET /container_sizes/new
  def new
    @container_size = ContainerSize.new
  end

  # GET /container_sizes/1/edit
  def edit
  end

  # POST /container_sizes
  # POST /container_sizes.json
  def create
    @container_size = ContainerSize.new(container_size_params)

    respond_to do |format|
      if @container_size.save
        format.html { redirect_to @container_size, notice: 'Container size was successfully created.' }
        format.json { render :show, status: :created, location: @container_size }
      else
        format.html { render :new }
        format.json { render json: @container_size.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /container_sizes/1
  # PATCH/PUT /container_sizes/1.json
  def update
    respond_to do |format|
      if @container_size.update(container_size_params)
        format.html { redirect_to @container_size, notice: 'Container size was successfully updated.' }
        format.json { render :show, status: :ok, location: @container_size }
      else
        format.html { render :edit }
        format.json { render json: @container_size.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /container_sizes/1
  # DELETE /container_sizes/1.json
  def destroy
    @container_size.destroy
    respond_to do |format|
      format.html { redirect_to container_sizes_url, notice: 'Container size was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_container_size
      @container_size = ContainerSize.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def container_size_params
      params.require(:container_size).permit(:code, :name)
    end
end
