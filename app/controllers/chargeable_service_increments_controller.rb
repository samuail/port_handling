class ChargeableServiceIncrementsController < ApplicationController

  # GET /chargeable_service_increments
  # GET /chargeable_service_increments.json
  def index
    @chargeable_service_increments = ChargeableServiceIncrement.all
    @response = {:success => true, :message => '', :data => @chargeable_service_increments}
    render json: @response
  end


  # POST /chargeable_service_increments
  # POST /chargeable_service_increments.json
  def create
    @chargeable_service_increment = ChargeableServiceIncrement.new(chargeable_service_increment_params)

    if @chargeable_service_increment.valid?
      @chargeable_service_increment.save
      @response = {:success => true, :message => 'Service Increment successfully', :data => @chargeable_service_increment}
      render json: @response
    else
      @response = {:success => false, :message => 'Error saving service increment', :data => []}
      render json: @response
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_chargeable_service_increment
      @chargeable_service_increment = ChargeableServiceIncrement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def chargeable_service_increment_params
      params.require(:chargeable_service_increment).permit(:chargeable_service_unit_of_charge_id, :service_increment_id)
    end
end
