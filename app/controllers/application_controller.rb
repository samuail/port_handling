require 'mkscommon/methodresponse'
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include ApplicationHelper

  def token
    authenticate_with_http_basic do |email, password|
      user = User.find_by(email: email)
      if user && user.authenticate(password)
        render json: { success: true, data: user.full_name, token: user.auth_token }
      else
        render json: {success: false, errors: "Invalid username or password"}
      end
    end
  end

  protected

  def verified_request?
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end
end
