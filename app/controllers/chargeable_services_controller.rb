class ChargeableServicesController < ApplicationController
  before_action :set_chargeable_service, only: [:update]

  # GET /chargeable_services
  # GET /chargeable_services.json
  def index
    @chargeable_services = ChargeableService.order(:order)
    chargeable_service_array = []
    @chargeable_services.each{ |chargeable_service|
      chargeable_service_array.push({:id => chargeable_service.id, :name => chargeable_service.name,
                                    :code => chargeable_service.code,
                                    :order => chargeable_service.order})
    }
    @response = {:success => true, :message => '', :data => chargeable_service_array}
    render json: @response
  end

  # POST /chargeable_services
  # POST /chargeable_services.json
  def create
    @chargeable_service = ChargeableService.new(chargeable_service_params)

    #respond_to do |format|
      #format.json do
        if @chargeable_service.valid?
          @chargeable_service.save
          @response = {:success => true, :message => 'Chargeable service recorded successfully', :data => @chargeable_service}
          render json: @response
        else
          @response = {:success => false, :message => 'Error saving chargeable service', :data => []}
          render json: @response
        end
      #end
    #end
  end

  # PATCH/PUT /chargeable_services/1
  # PATCH/PUT /chargeable_services/1.json
  def update
    @chargeable_service.update_attributes(chargeable_service_params)
    if @chargeable_service.save
      @response = {:success => true, :message => 'Chargeable service updated successfully', :data => @chargeable_service}
      render json: @response
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_chargeable_service
      @chargeable_service = ChargeableService.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def chargeable_service_params
      params.require(:chargeable_service).permit(:code, :name, :order)
    end
end
