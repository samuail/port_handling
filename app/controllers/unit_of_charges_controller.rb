class UnitOfChargesController < ApplicationController
  before_action :set_unit_of_charge, only: [:show, :edit, :update, :destroy]

  # GET /unit_of_charges
  # GET /unit_of_charges.json
  def index
    @unit_of_charges = UnitOfCharge.all
    @response = {:success => true, :message => '', :data => @unit_of_charges}
    render json: @response
  end

  # GET /unit_of_charges/1
  # GET /unit_of_charges/1.json
  def show
  end

  # GET /unit_of_charges/new
  def new
    @unit_of_charge = UnitOfCharge.new
  end

  # GET /unit_of_charges/1/edit
  def edit
  end

  # POST /unit_of_charges
  # POST /unit_of_charges.json
  def create
    @unit_of_charge = UnitOfCharge.new(unit_of_charge_params)

    respond_to do |format|
      if @unit_of_charge.save
        format.html { redirect_to @unit_of_charge, notice: 'Unit of charge was successfully created.' }
        format.json { render :show, status: :created, location: @unit_of_charge }
      else
        format.html { render :new }
        format.json { render json: @unit_of_charge.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /unit_of_charges/1
  # PATCH/PUT /unit_of_charges/1.json
  def update
    respond_to do |format|
      if @unit_of_charge.update(unit_of_charge_params)
        format.html { redirect_to @unit_of_charge, notice: 'Unit of charge was successfully updated.' }
        format.json { render :show, status: :ok, location: @unit_of_charge }
      else
        format.html { render :edit }
        format.json { render json: @unit_of_charge.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /unit_of_charges/1
  # DELETE /unit_of_charges/1.json
  def destroy
    @unit_of_charge.destroy
    respond_to do |format|
      format.html { redirect_to unit_of_charges_url, notice: 'Unit of charge was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_unit_of_charge
      @unit_of_charge = UnitOfCharge.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def unit_of_charge_params
      params.require(:unit_of_charge).permit(:code, :name)
    end
end
