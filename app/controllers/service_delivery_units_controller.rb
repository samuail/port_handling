class ServiceDeliveryUnitsController < ApplicationController
  before_action :service_delivery_unit, only: [:update]

  # GET /service_delivery_units
  # GET /service_delivery_units.json
  def index
    @service_delivery_units = ServiceDeliveryUnit.all
    service_delivery_units_array = []
    @service_delivery_units.each { |sdu|
      service_delivery_units_array.push({:id => sdu.id, :code => sdu.code, :name => sdu.name, :address => sdu.address,
                                        :currency_id => sdu.currency_id, :currency_name => sdu.currency.name,
                                        :service_delivery_unit_type_id => sdu.service_delivery_unit_type_id,
                                        :type_name => sdu.service_delivery_unit_type.name })
    }
    @response = {:success => true, :message => '', :data => service_delivery_units_array}
    render json: @response
  end

  # POST /service_delivery_units
  # POST /service_delivery_units.json
  def create
    @service_delivery_unit = ServiceDeliveryUnit.new(service_delivery_unit_params)

    if @service_delivery_unit.valid?
      @service_delivery_unit.save

      @response = {:success => true, :message => 'Service delivery unit recorded successfully', :data => @service_delivery_unit}
      render json: @response
    else
      @response = {:success => false, :message => 'Error saving service delivery unit', :data => []}
      render json: @response
    end
  end

  # PATCH/PUT /service_delivery_units/1
  # PATCH/PUT /service_delivery_units/1.json
  def update
    @service_delivery_unit.update_attributes(service_delivery_unit_params)
    if @service_delivery_unit.save
      @response = {:success => true, :message => 'Service delivery unit updated successfully', :data => @service_delivery_unit}
      render json: @response
    end
  end

  def chargeable_services
    service_delivery_unit_id = params[:id]
    chargeable_service_ids = ServiceDeliveryUnitService.select(:chargeable_service_id).where('service_delivery_unit_id' =>service_delivery_unit_id)
    chargeable_services = ChargeableService.where(id: chargeable_service_ids)
    @response = {:success => true, :message => '', :data => chargeable_services}
    render json: @response
  end

  def unassigned_services
    service_delivery_unit_id = params[:id]
    assigned_chargeable_services = ServiceDeliveryUnitService.select(:chargeable_service_id).where('service_delivery_unit_id' => service_delivery_unit_id)
    unassigned_chargeable_services = ChargeableService.where.not(id: assigned_chargeable_services)
    @response = {:success => true, :message => '', :data => unassigned_chargeable_services}
    render json: @response
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def service_delivery_unit
      @service_delivery_unit = ServiceDeliveryUnit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_delivery_unit_params
      params.require(:service_delivery_unit).permit(:code, :name, :address, :currency_id, :service_delivery_unit_type_id)
    end
end
