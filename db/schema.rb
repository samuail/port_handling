# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151213055724) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addendums", force: :cascade do |t|
    t.integer  "service_increment_id"
    t.integer  "addition_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "bill_of_loading_rate_archives", force: :cascade do |t|
    t.integer  "transaction_type_id"
    t.float    "low"
    t.float    "medium"
    t.float    "high"
    t.date     "effective_date"
    t.integer  "chargeable_service_unit_of_charge_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "bill_of_loading_rates", force: :cascade do |t|
    t.integer  "transaction_type_id"
    t.float    "low"
    t.float    "medium"
    t.float    "high"
    t.date     "effective_date"
    t.integer  "chargeable_service_unit_of_charge_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "break_bulk_archives", force: :cascade do |t|
    t.integer  "transaction_type_id"
    t.date     "effective_date"
    t.float    "low"
    t.float    "medium"
    t.float    "high"
    t.integer  "chargeable_service_unit_of_charge_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "break_bulk_rates", force: :cascade do |t|
    t.integer  "transaction_type_id"
    t.float    "low"
    t.float    "medium"
    t.float    "high"
    t.date     "effective_date"
    t.integer  "chargeable_service_unit_of_charge_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "break_bulk_unit_rates", force: :cascade do |t|
    t.integer  "chargeable_service_unit_of_charge_id"
    t.string   "transaction_type_id"
    t.integer  "break_bulk_unit_id"
    t.float    "high"
    t.float    "medium"
    t.float    "low"
    t.date     "effective_date"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "break_bulk_units", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "chargeable_service_increments", force: :cascade do |t|
    t.integer  "chargeable_service_unit_of_charge_id"
    t.integer  "service_increment_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "chargeable_service_rate_archives", force: :cascade do |t|
    t.integer  "container_size_id"
    t.integer  "transaction_type_id"
    t.float    "low"
    t.float    "medium"
    t.float    "high"
    t.date     "effective_date"
    t.integer  "chargeable_service_unit_of_charge_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "chargeable_service_unit_of_charges", force: :cascade do |t|
    t.integer  "service_delivery_unit_id"
    t.integer  "chargeable_service_id"
    t.integer  "unit_of_charge_id"
    t.boolean  "has_increment"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "chargeable_services", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.integer  "order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "container_sizes", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "container_types", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "currencies", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "increment_rates", force: :cascade do |t|
    t.float    "percent"
    t.date     "effective_date"
    t.integer  "service_increment_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "mks_application_modules", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mks_menus", force: :cascade do |t|
    t.string   "text"
    t.string   "icon_cls"
    t.string   "class_name"
    t.integer  "parent_id"
    t.string   "location"
    t.integer  "application_module_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "mks_menus_user_roles", id: false, force: :cascade do |t|
    t.integer "menu_id"
    t.integer "user_role_id"
  end

  add_index "mks_menus_user_roles", ["menu_id", "user_role_id"], name: "index_mks_menus_user_roles_on_menu_id_and_user_role_id", using: :btree

  create_table "mks_user_roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mks_users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.boolean  "active"
    t.integer  "application_module_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "password_digest"
  end

  create_table "mks_users_user_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "user_role_id"
  end

  add_index "mks_users_user_roles", ["user_id", "user_role_id"], name: "index_mks_users_user_roles_on_user_id_and_user_role_id", using: :btree

  create_table "rates", force: :cascade do |t|
    t.decimal  "rate_to_base"
    t.date     "rate_date"
    t.integer  "currency_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "service_delivery_unit_services", force: :cascade do |t|
    t.integer  "service_delivery_unit_id"
    t.integer  "chargeable_service_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "service_delivery_unit_types", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_delivery_units", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.integer  "currency_id"
    t.string   "address"
    t.integer  "service_delivery_unit_type_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "service_increments", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.boolean  "has_increment"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "service_rates", force: :cascade do |t|
    t.integer  "container_size_id"
    t.integer  "transaction_type_id"
    t.float    "low"
    t.float    "medium"
    t.float    "high"
    t.date     "effective_date"
    t.integer  "chargeable_service_unit_of_charge_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "transaction_types", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "truck_rates", force: :cascade do |t|
    t.float    "rate"
    t.date     "effective_date"
    t.integer  "transaction_type_id"
    t.integer  "chargeable_service_unit_of_charge_id"
    t.float    "low"
    t.float    "medium"
    t.float    "high"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "unit_of_charges", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
