class CreateBreakBulkUnits < ActiveRecord::Migration
  def change
    create_table :break_bulk_units do |t|
      t.string :code
      t.string :name

      t.timestamps null: false
    end
  end
end
