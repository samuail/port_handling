class CreateAddendums < ActiveRecord::Migration
  def change
    create_table :addendums do |t|
      t.integer :service_increment_id
      t.integer :addition_id

      t.timestamps null: false
    end
  end
end
