class CreateTruckRates < ActiveRecord::Migration
  def change
    create_table :truck_rates do |t|
      t.float :rate
      t.date :effective_date
      t.integer :transaction_type_id
      t.integer :chargeable_service_unit_of_charge_id
      t.float :low
      t.float :medium
      t.float :high

      t.timestamps null: false
    end
  end
end
