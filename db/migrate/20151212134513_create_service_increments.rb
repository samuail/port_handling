class CreateServiceIncrements < ActiveRecord::Migration
  def change
    create_table :service_increments do |t|
      t.string :code
      t.string :name
      t.boolean :has_increment

      t.timestamps null: false
    end
  end
end
