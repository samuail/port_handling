class CreateChargeableServiceUnitOfCharges < ActiveRecord::Migration
  def change
    create_table :chargeable_service_unit_of_charges do |t|
      t.integer :service_delivery_unit_id
      t.integer :chargeable_service_id
      t.integer :unit_of_charge_id
      t.boolean :has_increment

      t.timestamps null: false
    end
  end
end
