class CreateIncrementRates < ActiveRecord::Migration
  def change
    create_table :increment_rates do |t|
      t.float :percent
      t.date :effective_date
      t.integer :service_increment_id

      t.timestamps null: false
    end
  end
end
