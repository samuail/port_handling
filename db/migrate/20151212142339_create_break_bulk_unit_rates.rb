class CreateBreakBulkUnitRates < ActiveRecord::Migration
  def change
    create_table :break_bulk_unit_rates do |t|
      t.integer :chargeable_service_unit_of_charge_id
      t.string :transaction_type_id
      t.integer :break_bulk_unit_id
      t.float :high
      t.float :medium
      t.float :low
      t.date :effective_date

      t.timestamps null: false
    end
  end
end
