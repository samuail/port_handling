class CreateBreakBulkArchives < ActiveRecord::Migration
  def change
    create_table :break_bulk_archives do |t|
      t.integer :transaction_type_id
      t.date :effective_date
      t.float :low
      t.float :medium
      t.float :high
      t.integer :chargeable_service_unit_of_charge_id

      t.timestamps null: false
    end
  end
end
