class CreateServiceDeliveryUnitServices < ActiveRecord::Migration
  def change
    create_table :service_delivery_unit_services do |t|
      t.integer :service_delivery_unit_id
      t.integer :chargeable_service_id

      t.timestamps null: false
    end
  end
end
