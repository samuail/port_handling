class CreateChargeableServiceIncrements < ActiveRecord::Migration
  def change
    create_table :chargeable_service_increments do |t|
      t.integer :chargeable_service_unit_of_charge_id
      t.integer :service_increment_id

      t.timestamps null: false
    end
  end
end
