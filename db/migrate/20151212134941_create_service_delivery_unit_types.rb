class CreateServiceDeliveryUnitTypes < ActiveRecord::Migration
  def change
    create_table :service_delivery_unit_types do |t|
      t.string :code
      t.string :name

      t.timestamps null: false
    end
  end
end
