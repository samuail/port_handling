class CreateContainerSizes < ActiveRecord::Migration
  def change
    create_table :container_sizes do |t|
      t.string :code
      t.string :name

      t.timestamps null: false
    end
  end
end
