class CreateChargeableServices < ActiveRecord::Migration
  def change
    create_table :chargeable_services do |t|
      t.string :code
      t.string :name
      t.integer :order

      t.timestamps null: false
    end
  end
end
