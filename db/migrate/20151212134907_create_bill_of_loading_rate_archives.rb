class CreateBillOfLoadingRateArchives < ActiveRecord::Migration
  def change
    create_table :bill_of_loading_rate_archives do |t|
      t.integer :transaction_type_id
      t.float :low
      t.float :medium
      t.float :high
      t.date :effective_date
      t.integer :chargeable_service_unit_of_charge_id

      t.timestamps null: false
    end
  end
end
