class CreateServiceDeliveryUnits < ActiveRecord::Migration
  def change
    create_table :service_delivery_units do |t|
      t.string :code
      t.string :name
      t.integer :currency_id
      t.string :address
      t.integer :service_delivery_unit_type_id

      t.timestamps null: false
    end
  end
end
