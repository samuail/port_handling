class CreateUnitOfCharges < ActiveRecord::Migration
  def change
    create_table :unit_of_charges do |t|
      t.string :code
      t.string :name

      t.timestamps null: false
    end
  end
end
