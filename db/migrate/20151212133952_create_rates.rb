class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.decimal :rate_to_base
      t.date :rate_date
      t.integer :currency_id

      t.timestamps null: false
    end
  end
end
