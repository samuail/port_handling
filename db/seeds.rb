# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Mks::Core::Engine.load_seed

menu1 = Mks::Menu.where(text: 'System Settings', icon_cls: 'fa fa-cogs fa-lg').first_or_create
menu2 = Mks::Menu.where(text: 'Business Settings', icon_cls: 'fa fa-cog fa-lg').first_or_create

owner_role = Mks::UserRole.find_by :name => 'Owner'
staff_role = Mks::UserRole.find_by :name => 'Staff'

app_module = Mks::ApplicationModule.find_by(:code => 'PSH')

Mks::Menu.where(text: 'Users', parent_id: menu1.id, location: '', icon_cls: 'xf007', application_module_id: app_module.id).first_or_create
Mks::Menu.where(text: 'Container Sizes', parent_id: menu1.id, location: '', icon_cls: 'xf0c9', application_module_id: app_module.id).first_or_create
Mks::Menu.where(text: 'Container Types', parent_id: menu1.id, location: '', icon_cls: 'xf1ea', application_module_id: app_module.id).first_or_create
Mks::Menu.where(text: 'Currencies', parent_id: menu1.id, location: '', icon_cls: 'xf155', application_module_id: app_module.id).first_or_create
Mks::Menu.where(text: 'Currency Rates', parent_id: menu1.id, location: '', icon_cls: 'xf073', application_module_id: app_module.id).first_or_create
Mks::Menu.where(text: 'Transaction Types', parent_id: menu1.id, location: '', icon_cls: 'xf0ec', application_module_id: app_module.id).first_or_create
Mks::Menu.where(text: 'Units of Charge', parent_id: menu1.id, location: '', icon_cls: 'xf029', application_module_id: app_module.id).first_or_create
Mks::Menu.where(text: 'Break Bulk Units', parent_id: menu1.id, class_name: 'break-bulk-unit-grid', icon_cls: 'xf1cb', application_module_id: app_module.id).first_or_create

Mks::Menu.where(text: 'Service Delivery Units', parent_id: menu2.id, class_name: 'service-delivery-unit-grid',
           icon_cls: 'xf029', application_module_id: app_module.id).first_or_create
Mks::Menu.where(text: 'Chargeable Services', parent_id: menu2.id, class_name: 'chargeable-service-grid', icon_cls: 'xf1ec', application_module_id: app_module.id)
    .first_or_create
Mks::Menu.where(text: 'Increments', parent_id: menu2.id, class_name: 'increment-grid', icon_cls: 'xf077', application_module_id: app_module.id).first_or_create

Mks::Menu.where(text: 'Unit of Charge for Services', parent_id: menu2.id, class_name: 'chargeable-service-unit-of-charge-grid', icon_cls: 'xf044', application_module_id: app_module.id).first_or_create

Mks::Menu.where(text: 'Rates (Containerized)', parent_id: menu2.id, class_name: 'chargeable-service-rate-grid',
           icon_cls: 'xf0f7', application_module_id: app_module.id).first_or_create
Mks::Menu.where(text: 'Rates (Break Bulk [FT])', parent_id: menu2.id, class_name: 'break-bulk-rate-grid', icon_cls: 'xf0f7', application_module_id: app_module.id)
    .first_or_create
Mks::Menu.where(text: 'Rates (Break Bulk [Unit])', parent_id: menu2.id, class_name: 'break-bulk-unit-rate-grid', icon_cls: 'xf0f7', application_module_id: app_module.id)
    .first_or_create
Mks::Menu.where(text: 'Rates (Bill of Loading)', parent_id: menu2.id, class_name: 'bill-of-loading-rate-grid', icon_cls: 'xf0f7', application_module_id: app_module.id)
    .first_or_create
Mks::Menu.where(text: 'Truck Rate', parent_id: menu2.id, class_name: 'truck-rate-grid', icon_cls: 'xf0d1', application_module_id: app_module.id).first_or_create

owner_role.menus.clear
owner_role.menus << menu1
owner_role.menus << menu1.children

staff_role.menus.clear
staff_role.menus << menu2
staff_role.menus << menu2.children