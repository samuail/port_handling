Rails.application.routes.draw do
  mount Mks::Core::Engine => '/auth', as: 'mks_core'

  resources :break_bulk_unit_rates, except: [:new, :edit]
  resources :break_bulk_units, except: [:new, :edit]
  resources :chargeable_service_unit_of_charges do
    member do
      get 'increments'
      get 'unassigned_increments'
    end
  end

  post 'chargeable_service_unit_of_charges/update_changes', :controller => 'chargeable_service_unit_of_charges',
       :action => 'update_changes'

  resources :truck_rates

  resources :break_bulk_rates do
    member do
      get 'get_chargeable_service_for_service_delivery_unit/:service_delivery_unit_id', :action => 'get_chargeable_service_for_service_delivery_unit'
      get 'get_rate_for_chargeable_service/:chargeable_service_id', :action => 'get_rate_for_chargeable_service'
    end
  end
  resources :service_delivery_unit_services
  resources :service_delivery_unit_types

  resources :service_rates do
    member do
      get 'get_chargeable_services_for_service_delivery_unit/:service_delivery_unit_id', :action => 'get_chargeable_services_for_service_delivery_unit'
      get 'get_rate_for_chargeable_service/:chargeable_service_id', :action => 'get_rate_for_chargeable_service'
    end
  end

  resources :chargeable_service_increments
  get 'increment_rates/get_all'
  get 'addendums/get_all'
  resources :service_increments do
    member do
      get 'increments'
      get 'unassigned_increments'
    end
    resources :increment_rates
    resources :addendums
  end
  resources :container_sizes
  resources :container_types
  resources :transaction_types
  resources :unit_of_charges
  resources :service_delivery_units do
    member do
      get 'chargeable_services'
      get 'unassigned_services'
    end
  end
  resources :bill_of_loading_rates do
    member do
      get 'get_chargeable_service_for_service_delivery_unit/:service_delivery_unit_id', :action => 'get_chargeable_service_for_service_delivery_unit'
      get 'get_rate_for_chargeable_service/:chargeable_service_id', :action => 'get_rate_for_chargeable_service'
    end
  end
  resources :chargeable_services

  resources :currencies
  resources :rates

end
