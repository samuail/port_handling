/**
 * Created by betse on 6/5/2015.
 */
Ext.define('PH.controller.Menu', {
  extend: 'Ext.app.Controller',
  requires: ['PH.view.menu.Tree'],
  stores: [
    'menu.Menu'
  ],
  views: [
      'breakbulkunit.BreakBulkUnitGrid',
    //'chargeableservice.ChargeableServiceGrid',
    //'increment.IncrementGrid',
    //'billofloading.BillOfLoadingGrid',
    //'quotationitem.QuotationItemGrid',
    //'chargeableservicerate.ChargeableServiceRateGrid',
    //'billofloadingrate.BillOfLoadingRateGrid',
    'servicedeliveryunit.ServiceDeliveryUnitGrid'
    //'breakbulkrate.BreakBulkRateGrid',
    //'truckrate.TruckRateGrid',
    //'template.TemplateGrid',
    //'mail.InboxGrid',
    //'mail.SentGrid',
    ////'coverletter.CoverLetterGrid',
    ////'address.AddressGrid',
    //'mail.ComposeMailPage',
    //'printrequest.PrintForm',
    //'quotationitemapproval.QuotationItemApprovalGrid',
    //'routelookup.RouteLookupGrid',
    //'route.RouteGrid',
    //'transporter.TransporterGrid',
    //'trucktypeassignment.TruckTypeAssignmentGrid',
    //'routerate.RouteRateGrid',
    //'client.ClientGrid',
    //'coverletter.CoverLetterGrid'
  ],

  init: function () {
    this.control({
      "menu-tree": {
        itemclick: this.onTreePanelItemClick
      },
      "main-menu": {
        render: this.renderDynamicMenu
      }
    });
  },
  refs: [
    {
      ref: 'mainPanel',
      selector: 'mainpanel'
    }
  ],
  renderDynamicMenu: function (view) {
    var dynamicMenus = [];
    view.body.mask("Loading menus.....please wait......");
    this.getMenuMenuStore().load(function (records, op, success) {
      Ext.each(records, function (root) {
        var menu = Ext.create('PH.view.menu.Tree', {
          title: root.get('text'),
          iconCls: root.get('iconCls')
        });
        var treeNodeStore = root.children(),
            nodes = [],
            item;
        for (var i = 0; i < treeNodeStore.getCount(); i++) {
          item = treeNodeStore.getAt(i);

          nodes.push({
            text: item.get('text'),
            leaf: true,
            glyph: item.get('iconCls'),
            id: item.get('id'),
            className: item.get('className')
          });
        }

        menu.getRootNode().appendChild(nodes);
        dynamicMenus.push(menu);
      });
      view.add(dynamicMenus);
      view.body.unmask();
    });
  },
  onTreePanelItemClick: function (view, record) {
    "use strict";
    var mainPanel = this.getMainPanel(),
        newTab = mainPanel.items.findBy(
            function (tab) {
              return tab.title === record.get('text');
            }
        );

    if (!newTab) {
      newTab = mainPanel.add({
        xtype: record.get('className'),
        closable: true,
        glyph: record.get('glyph') + '@FontAwesome',
        iconCls: 'tabIcon',
        title: record.get('text')
      });
    }
    mainPanel.setActiveTab(newTab);
  }
});
