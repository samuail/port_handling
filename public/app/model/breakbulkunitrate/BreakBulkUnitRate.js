/**
 * Created by betse on 12/12/15.
 */
Ext.define('PH.model.breakbulkunitrate.BreakBulkUnitRate',{
    extend: 'Ext.data.Model',
    requires:['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'chargeable_service_unit_of_charge_id'},
        {name: 'service_delivery_unit_name'},
        {name: 'service_delivery_unit_id'},
        {name: 'chargeable_service_id'},
        {name: 'chargeable_service_name'},
        {name: 'transaction_type_id'},
        {name: 'transaction_type_name'},
        {name: 'break_bulk_unit_id'},
        {name: 'break_bulk_unit_name'},
        {name: 'high'},
        {name: 'medium'},
        {name: 'low'},
        {name: 'effective_date'}
    ],
    proxy:{
        type: 'baseproxy',
        api:{
            read: '/break_bulk_unit_rates'
        }
    }
});
