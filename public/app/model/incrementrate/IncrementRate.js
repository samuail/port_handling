/**
 * Created by betse on 10/6/15.
 */
Ext.define('PH.model.incrementrate.IncrementRate',{
    extend: 'Ext.data.Model',
    requires: ['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields: [
        {name: 'foo', persist:false},
        {name: 'id'},
        {name: 'percent'},
        {name: 'effective_date'},
        {name: 'service_increment_id'}
    ],
    proxy:{
        type: 'baseproxy',
        url: '/increment_rates/get_all'
    }
});
