/**
 * Created by betse on 12/8/15.
 */
Ext.define('PH.model.csuoc.ChargeableServiceUnitOfCharge',{
    extend: 'Ext.data.Model',
    requires:['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'service_delivery_unit_id'},
        {name: 'service_delivery_unit_name'},
        {name: 'chargeable_service_name'},
        {name: 'Chargeable_service_id'},
        {name: 'unit_of_charge_id'},
        {name: 'unit_of_charge_name'},
        {name: 'has_increment'}
    ],
    proxy:{
        type: 'baseproxy',
        url: '/chargeable_service_unit_of_charges'
    }
});
