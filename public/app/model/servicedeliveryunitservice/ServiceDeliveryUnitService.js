/**
 * Created by betse on 10/20/15.
 */
Ext.define('PH.model.servicedeliveryunitservice.ServiceDeliveryUnitService',{
    extend: 'Ext.data.Model',
    requires:['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'chargeable_service_id'},
        {name: 'chargeable_service_name'},
        {name: 'service_delivery_unit_id'},
        {name: 'service_delivery_unit_name'}
    ],
    proxy:{
        type: 'baseproxy'
    }
});
