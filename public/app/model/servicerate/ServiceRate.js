/**
 * Created by betse on 10/1/15.
 */
Ext.define('PH.model.servicerate.ServiceRate',{
    extend: 'Ext.data.Model',
    requires:['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'container_size_id'},
        {name: 'container_size_name'},
        {name: 'transaction_type_id'},
        {name: 'transaction_type_name'},
        {name: 'service_delivery_unit_name'},
        {name: 'service_delivery_unit_id'},
        {name: 'chargeable_service_id'},
        {name: 'chargeable_servicename'},
        {name: 'high'},
        {name: 'medium'},
        {name: 'low'},
        {name: 'effective_date'}
    ],
    proxy:{
        type: 'baseproxy',
        api:{
            read: '/service_rates'
        }
    }
});
