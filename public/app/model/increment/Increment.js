/**
 * Created by betse on 9/30/15.
 */
Ext.define('PH.model.increment.Increment',{
    extend: 'Ext.data.Model',
    requires:['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'code'},
        {name: 'name'}
    ],
    proxy:{
        type: 'baseproxy',
        api:{
            read: '/service_increments'
        }
    }
});
