/**
 * Created by betse on 10/30/15.
 */
Ext.define('PH.model.truckrate.TruckRate',{
    extend: 'Ext.data.Model',
    requires:['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'service_delivery_unit_id'},
        {name: 'service_delivery_unit_name'},
        {name: 'chargeable_service_id'},
        {name: 'chargeable_service_name'},
        {name: 'transaction_type_id'},
        {name: 'transaction_type_name'},
        {name: 'low'},
        {name: 'medium'},
        {name: 'high'},
        {name: 'effective_date'},
        {name: 'rate'}
    ],
    proxy:{
        type: 'baseproxy',
        api:{
            read: '/truck_rates'
        }
    }
});