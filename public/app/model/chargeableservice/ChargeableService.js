/**
 * Created by betse on 9/28/15.
 */
Ext.define('PH.model.chargeableservice.ChargeableService',{
    extend: 'Ext.data.Model',
    requires:['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'code'},
        {name: 'name'},
        {name: 'has_increment'},
        {name: 'order'}
    ],
    proxy:{
        type: 'baseproxy',
        api:{
            read:'/chargeable_services'
        }
    }
});
