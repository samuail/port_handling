/**
 * Created by betse on 10/20/15.
 */
Ext.define('PH.model.servicedeliveryunit.ServiceDeliveryUnit',{
    extend: 'Ext.data.Model',
    requires:['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'code'},
        {name: 'name'},
        {name: 'address'},
        {name: 'currency_id'},
        {name: 'currency_name'},
        {name: 'service_delivery_unit_type_id'},
        {name: 'type_name'}
    ],
    proxy:{
        type: 'baseproxy',
        api:{
            read: '/service_delivery_units'
        }
    }
});
