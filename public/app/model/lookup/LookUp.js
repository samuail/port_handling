/**
 * Created by betse on 10/4/15.
 */
Ext.define('PH.model.lookup.LookUp',{
    extend: 'Ext.data.Model',
    requires: ['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'code'},
        {name: 'name'}
    ],
    proxy:{
        type: 'baseproxy'
    }
});