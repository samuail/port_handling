/**
 * Created by betse on 10/11/15.
 */
Ext.define('PH.model.billofloadingrate.BillOfLoadingRate',{
    extend: 'Ext.data.Model',
    requires:['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'chargeable_service_id'},
        {name: 'chargeable_service_name'},
        {name: 'transaction_type_id'},
        {name: 'transaction_type_name'},
        {name: 'service_delivery_unit_name'},
        {name: 'service_delivery_unit_id'},
        {name: 'low'},
        {name: 'medium'},
        {name: 'high'}
    ],
    proxy:{
        type: 'baseproxy',
        api:{
            read: '/bill_of_loading_rates'
        }
    }
});
