/**
 * Created by betse on 5/16/2015.
 */
Ext.define('PH.model.menu.Accordion',{
    extend: 'Ext.data.Model',
    requires: ['PH.model.menu.TreeNode'],
    fields:[
        {name: 'id', type: 'int'},
        {name: 'text'},
        {name: 'iconCls'}
    ],
    hasMany:{
        model: 'PH.model.menu.TreeNode',
        foreignKey: 'parentId',
        name: 'children'
    }
});
