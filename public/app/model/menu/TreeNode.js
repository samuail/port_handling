/**
 * Created by betse on 5/16/2015.
 */
Ext.define('PH.model.menu.TreeNode',{
    extend: 'Ext.data.Model',
    fields:[
        {name: 'id', type: 'int'},
        {name: 'text'},
        {name: 'iconCls'},
        {name: 'className'},
        {name: 'parentId'}
    ]
});


