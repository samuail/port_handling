/**
 * Created by betse on 11/14/15.
 */

Ext.define('PH.model.chargeableserviceincrement.ChargeableServiceIncrement',{
    extend: 'Ext.data.Model',
    requires:['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'chargeable_service_id'},
        {name: 'service_increment_id'}
    ],
    proxy:{
        type: 'baseproxy',
        api:{
            read:'/chargeable_service_increments'
        }
    }
});