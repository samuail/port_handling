/**
 * Created by betse on 11/14/15.
 */
Ext.define('PH.model.addition.Addition',{
    extend: 'Ext.data.Model',
    requires:['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'service_increment_id'},
        {name: 'addition_id'}
    ],
    proxy:{
        type: 'baseproxy',
        api:{
            read: '/addendums/get_all'
        }
    }
});