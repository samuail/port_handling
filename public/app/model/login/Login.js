/**
 * Created by betse on 5/22/2015.
 */
Ext.define('PH.model.login.Login', {
  extend: 'Ext.data.Model',
  Fields: [
    {name: 'email'},
    {name: 'password'},
    {name: 'module_code'}
  ]
});
