/**
 * Created by betse on 10/20/15.
 */
Ext.define('PH.model.currency.Currency',{
    extend: 'Ext.data.Model',
    requires:['CM.proxy.BaseProxy'],
    idProperty: 'foo',
    fields:[
        {name: 'foo', persist: false},
        {name: 'id'},
        {name: 'code'},
        {name: 'name'}
    ],
    proxy:{
        type: 'baseproxy',
        api:{
            read: '/currencies'
        }
    }
});
