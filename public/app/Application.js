/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define('PH.Application', {
  extend: 'Ext.app.Application',
  requires: [
    'PH.view.login.Login',
    'PH.overrides.tree.ColumnOverride',
    'PH.util.CsrfTokenHelper',
    'CM.util.Glyphs',
    'CM.util.Util',
    'PH.model.login.Login'
  ],
  glyphFontFamily: 'FontAwesome',
  name: 'PH',

  controllers: [
    'Menu'
  ],

  stores: [
    // TODO: add global / shared stores here
  ],

  launch: function () {
    var label;
    Ext.Ajax.request({
      url: '/auth/check_login',
      success: function (conn) {
        var result = CM.util.Util.decodeJSON(conn.responseText)
        if (result.success) {
          Ext.create('PH.view.main.Main');
          label = Ext.ComponentQuery.query('container appheader label#userLabel')[0];
          label.setHtml('<span style="font-size: 12px">Logged in user: <b>' + result.data + '</b></span>');
        } else {
          var me = this;
          me.splashscreen = Ext.getBody().mask(
            'Loading Application', 'splashscreen'
          );
          Ext.DomHelper.insertFirst(Ext.query('.x-mask-msg')[0], {
            cls: 'x-splash-icon'
          });
          Ext.Ajax.request({
            url: '/auth/csrf_token',

            success: function () {
              var csrftoken = Ext.util.Cookies.get('csrftoken')
            }
          });

          //var me = this;
          var task = new Ext.util.DelayedTask(function () {
            me.splashscreen.fadeOut({
              duration: 1000,
              remove: true,
              listeners: {
                afteranimate: function () {
                  Ext.widget('login-window');
                }
              }
            });
          });
          task.delay(2000);
        }
      }
    });
  }
});
