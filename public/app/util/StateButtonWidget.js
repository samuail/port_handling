/**
 * Created by henock on 11/22/15.
 */
Ext.define('QM.util.StateButtonWidget', {
  extend: 'Ext.grid.column.Widget',
  alias: 'widget.statebuttonwidget',
  onUpdate: null,

  onWidgetAttach: function(column, widget, record) {
    var me = this,
        fn = me.onUpdate;

    if (fn) {
      fn.call(widget, record, column);

      me.mon(me.up('gridpanel').getView(), {
        scope: widget,
        args: [record, column],
        itemupdate: fn
      });
    }
  }
});