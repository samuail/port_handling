Ext.define('PH.util.CsrfTokenHelper', {
	singleton: true,

	constructor: function() {
		Ext.Ajax.on('beforerequest', function (conn, options) {
			if (!(/^http:.*/.test(options.url) || /^https:.*/.test(options.url))) {
				if (typeof(options.headers) == "undefined") {
					options.headers = {
						'Accept': 'application/json',
						'X-XSRF-TOKEN': Ext.util.Cookies.get('XSRF-TOKEN')
					};
				} else {
					options.headers['Application'] = 'application/json';
					options.headers['X-XSRF-TOKEN'] = Ext.util.Cookies.get('XSRF-TOKEN');
				}
			}
		}, this);
	}
});
