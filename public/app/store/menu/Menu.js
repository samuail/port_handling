/**
 * Created by betse on 5/16/2015.
 */
Ext.define('PH.store.menu.Menu',{
    extend: 'Ext.data.Store',
    alias:'store.menu',
    requires: ['PH.model.menu.Accordion'],
    model: 'PH.model.menu.Accordion',
    proxy: {
        type: 'ajax',
        url: '/auth/menu',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        listeners: {
            exception: function (proxy, response) {
                console.log(response.responseText);
            }
        }
    }
});
