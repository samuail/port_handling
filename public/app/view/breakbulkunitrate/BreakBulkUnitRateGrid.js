/**
 * Created by betse on 12/12/15.
 */
Ext.define('PH.view.breakbulkunitrate.BreakBulkUnitRateGrid',{
    extend: 'Ext.container.Container',
    alias: 'widget.break-bulk-unit-rate-grid',
    requires: [
        'PH.view.breakbulkunitrate.BreakBulkUnitRateModel',
        'PH.view.breakbulkunitrate.BreakBulkUnitRateController',
        'Ext.selection.CellModel',
        'Ext.grid.filters.Filters',
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.util.*',
        'Ext.form.*'
    ],
    controller: 'break-bulk-unit-rate',
    viewModel: {
        type: 'break-bulk-unit-rate'
    },
    width: '100%',
    layout: 'vbox',
    items:[
        {
            xtype: 'fieldset',
            title: 'Rate controls',
            layout: 'hbox',
            width: '100%',
            margin: '5 0 0 5',
            items:[
                {
                    xtype: 'datefield',
                    fieldLabel: 'Effective Date',
                    reference: 'effectiveDate',
                    margin: '5 30 5 5'
                },
                {
                    xtype: 'button',
                    text: 'Generate',
                    handler: 'onGenerateButtonClick',
                    margin: '5 30 5 5'
                },
                {
                    xtype: 'button',
                    text: 'Update',
                    handler: 'onUpdateButtonClick',
                    margin: '5 5 5 5'
                }
            ]
        },
        {
            xtype: 'grid',
            width: '100%',
            flex: 1,
            columnLines: true,
            plugins: [
                'gridfilters',
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 1
                })
            ],
            selModel: {
                selType: 'cellmodel'
            },
            bind: '{breakBulkUnitRates}',
            reference: 'breakBulkUnitRatesGrid',
            columns:[
                {
                    text: 'Service Delivery Unit',
                    dataIndex: 'service_delivery_unit_name',
                    flex: 1,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                {
                    text: 'Chargeable Service Name',
                    dataIndex: 'chargeable_service_name',
                    flex: 2,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                {
                    text: 'Transaction Type',
                    dataIndex: 'transaction_type_name',
                    flex: 1,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                {
                    text: 'Break Bulk Unit',
                    dataIndex: 'break_bulk_unit_name',
                    flex: 1,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                {
                    text: 'Low Rate',
                    dataIndex: 'low',
                    flex: 1,
                    editor:{
                        xtype: 'numberfield',
                        allowBlank: false
                    }
                },
                {
                    text: 'Medium Rate',
                    dataIndex: 'medium',
                    flex: 1,
                    editor:{
                        xtype: 'numberfield',
                        allowBlank: false
                    }
                },
                {
                    text: 'High Rate',
                    dataIndex: 'high',
                    flex: 1,
                    editor:{
                        xtype: 'numberfield',
                        allowBlank: false
                    }
                },

                {
                    text: 'Effective Date',
                    dataIndex: 'effective_date',
                    flex: 1
                }
            ]
        }
    ]
});
