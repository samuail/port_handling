/**
 * Created by betse on 12/12/15.
 */
Ext.define('PH.view.breakbulkunitrate.BreakBulkUnitRateModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.break-bulk-unit-rate',
    requires:[
        'PH.model.breakbulkunitrate.BreakBulkUnitRate'
    ],

    stores:{
        breakBulkUnitRates:{
            model: 'PH.model.breakbulkunitrate.BreakBulkUnitRate',
            autoLoad: true
        }
    }
});