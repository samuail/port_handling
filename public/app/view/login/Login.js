/**
 * Created by betse on 5/2/2015.
 */
Ext.define('PH.view.login.Login', {
  extend: 'Ext.window.Window',
  xtype: 'login-window',
  requires: [
    'PH.view.login.LoginController',
    'PH.view.login.LoginModel'
  ],
  controller: 'login',
  viewModel: {
    type: 'login'
  },
  autoShow: true,
  height: 170,
  width: 360,
  layout: 'fit',
  iconCls: 'fa fa-key fa-lg',
  title: 'Login',
  closableAction: 'hide',
  closable: false,
  resizeable: false,
  items: [
    {
      xtype: 'form',
      bodyPadding: 15,
      reference: 'loginForm',
      defaults: {
        xtype: 'textfield',
        anchor: '100%',
        labelWidth: 120,
        allowBlank: false,
        minLength: 3,
        msgTarget: 'under'
      },
      items: [
        {
          name: 'email',
          bind: '{theLogin.email}',
          fieldLabel: 'Email',
          vtype: 'email',
          maxLength: 60,
          listeners: {
            specialkey: 'onTextFieldSpecialKey'
          }
        },
        {
          inputType: 'password',
          bind: '{theLogin.password}',
          name: 'password',
          fieldLabel: 'Password',
          maxLength: 60,
          listeners: {
            specialkey: 'onTextFieldSpecialKey'
          }
        },
        {
          xtype: 'hiddenfield',
          bind: '{theLogin.module_code}',
          //value: 'PSM',
          name: 'module_code'
        }
      ],
      buttons: [
        {
          text: 'Login',
          iconCls: 'fa fa-sign-in fa-lg',
          listeners: {
            click: 'onLoginClick'
          }
        },
        {
          text: 'Cancel',
          iconCls: 'fa fa-times fa-lg',
          listeners: {
            click: 'onCancelClick'
          }
        }
      ]
    }
  ]
});
