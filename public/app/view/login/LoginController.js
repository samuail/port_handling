/**
 * Created by betse on 5/3/2015.
 */
Ext.define('PH.view.login.LoginController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.login',
  requires: [
    'PH.view.main.Main'
  ],

  onCancelClick: function (button, e, options) {
    this.lookupReference('loginForm').reset();
  },
  onLoginClick: function () {
    "use strict";
    var form = this.lookupReference('loginForm'),
        rec,
        win = this.getView();
    if (form.isValid()) {
      rec = this.getViewModel().get('theLogin').data;
      win.getEl().mask('Authenticating... Please wait...', 'loading');

      Ext.Ajax.request({
        url: '/auth/login',
        params: rec,

        failure: function (conn, response, options, eOpts) {
          win.getEl().unmask();
          CM.util.Util.showErrorMsg(conn.responseText);
        },
        success: function (conn, response, options, eOpts) {
          win.getEl().unmask();
          var result = CM.util.Util.decodeJSON(conn.responseText),
              label;

          if (result.success) {
            win.close();
            Ext.create('PH.view.main.Main');
            label = Ext.ComponentQuery.query('container appheader label#userLabel')[0];
            label.setHtml('<span style="font-size: 12px">Logged in user: <b>' + result.data + '</b></span>');
            //CM.util.SessionMonitor.start();
          } else {
            CM.util.Util.showErrorMsg(result.errors);
          }
        }
      });
    }
  },
  onTextFieldSpecialKey: function (field, e, options) {
    "use strict";
    if (e.getKey() === e.ENTER) {
      this.onLoginClick();
    }
  }
});
