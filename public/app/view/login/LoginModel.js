/**
 * Created by betse on 5/22/2015.
 */
Ext.define('PH.view.login.LoginModel', {
  extend: 'Ext.app.ViewModel',
  requires: ['PH.model.login.Login'],
  alias: 'viewmodel.login',
  data: {
    theLogin: Ext.create('PH.model.login.Login', {
      module_code: 'PSH'
    })
  }
  //links: {
  //  theLogin: {
  //    type: 'PH.model.login.Login',
  //    create: true
  //  }
  //}
});
