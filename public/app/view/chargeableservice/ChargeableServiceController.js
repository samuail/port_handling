/**
 * Created by betse on 9/28/15.
 */
Ext.define('PH.view.chargeableservice.ChargeableServiceController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.chargeable-service',
    requires:[
        'PH.view.chargeableservice.ChargeableServiceWindow',
        'PH.view.servicerate.ServiceRateView',
        'PH.view.assignment.AssignmentView',
        'CM.util.Util'
    ],

    createDialog: function(record){
        var me = this,
            view = me.getView();
        this.dialog = view.add({
            xtype: 'chargeable-service-window',
            viewModel:{
                data: {
                    title: record ? 'Edit '+ record.get('name') : 'Add chargeable service'
                },
                links:{
                    theChargeableService: record||{
                        type: 'PH.model.chargeableservice.ChargeableService',
                        create: true
                    }
                }
            }
        })
        view.up().up().mask();
        this.dialog.show();
    },
    onAddButtonClick: function(){
        "use strict";
        this.createDialog(null);
    },
    onEditButtonClick: function () {
        "use strict";
        var me = this,
            grid = me.getView(),
            selection = grid.getSelectionModel().getSelection();

        if (selection && selection.length > 0) {
            if (selection.length > 1) {
                CM.util.Util.showInfoMsg('Please select only one item to edit');
            } else {
                this.createDialog(selection[0]);
            }
        } else {
            CM.util.Util.showInfoMsg('You have to select a record to edit!');
        }
    },

    onSaveButtonClick: function () {
        "use strict";
        var me = this,
            form = me.lookupReference('chargeableServiceForm'),
            url,
            method,
            chargeableService,
            data;
        if(form.isValid())
        {
            chargeableService = this.getViewModel().get("theChargeableService").getData();
            data = {'chargeable_service': chargeableService};
            if(this.getViewModel().get("theChargeableService").phantom == true){
                url = '/chargeable_services';
                method = 'POST'
            }
            else{
                url = '/chargeable_services/'+ chargeableService.id;
                method = 'PUT'
            }
            Ext.Ajax.request({
                method: method,
                url: url,
                headers: { 'Content-Type': 'application/json' },
                jsonData:data,
                success: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    if (result.success) {
                        me.getStore('chargeableServices').load();
                        me.onCancelButtonClick();
                        CM.util.Util.showToast(result.message);
                    } else {
                        CM.util.Util.showErrorMsg(result.message);
                    }
                },
                failure: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    CM.util.Util.showErrorMsg(result.message);
                }
            });
        }
    },

    /*onRatesButtonClick: function(btn){
        "use strict";
        var record = btn.getWidgetRecord(),
            serviceRateStore,
            containerTypeStore,
            containerSizeStore,
            transactionTypeStore,
            portStore,
            chargeableServiceId = record.get('id'),
            win = Ext.create('PH.view.servicerate.ServiceRateView',{title: record.get('name') + ' Rates'});
        serviceRateStore = win.getViewModel().get('serviceRates');
        containerTypeStore = win.getViewModel().get('containerTypes');
        containerSizeStore = win.getViewModel().get('containerSizes');
        transactionTypeStore = win.getViewModel().get('transactionTypes');
        portStore = win.getViewModel().get('ports');
        serviceRateStore.load({
            url: '/chargeable_services/'+ chargeableServiceId +'/service_rates'
        });
        containerTypeStore.load({url: '/container_types'});
        containerSizeStore.load({url: '/container_sizes'});
        transactionTypeStore.load({url: '/transaction_types'});
        portStore.load({url: '/ports'});
        win.lookupReference('chargeable_service_id').setValue(chargeableServiceId);
        win.show();
    },
*/
   /* onIncrementButtonClick: function(btn){
        "use strict";
        var record = btn.getWidgetRecord(),
            applicableIncrementStore,
            incrementsStore,
            chargeableServiceUnitOfChargeId = record.get('id'),
            win = Ext.create('PH.view.assignment.AssignmentView', {title: record.get('name') + ' Increments'});
        applicableIncrementStore = win.getViewModel().get('applicableIncrements');
        incrementsStore = win.getViewModel().get('increments');
        applicableIncrementStore.load({
            url: '/chargeable_service_unit_of_charges/'+ chargeableServiceUnitOfChargeId +'/increments'
        });
        incrementsStore.load({
            url: '/chargeable_service_unit_of_charges/'+ chargeableServiceUnitOfChargeId +'/unassigned_increments'
        });
        win.lookupReference('chargeable_service_unit_of_charge_id').setValue(chargeableServiceUnitOfChargeId);
        win.show();
    },
*/
    onCancelButtonClick: function(){
        "use strict";
        var view = this.getView();
        view.up().up().getEl().unmask();
        this.dialog = Ext.destroy(this.dialog);
    }
});
