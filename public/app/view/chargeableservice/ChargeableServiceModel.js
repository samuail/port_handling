/**
 * Created by betse on 9/28/15.
 */
Ext.define('PH.view.chargeableservice.ChargeableServiceModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.chargeable-service',
    requires: [
        'PH.model.chargeableservice.ChargeableService'
    ],

    stores:{
        chargeableServices:{
            model: 'PH.model.chargeableservice.ChargeableService',
            autoLoad: true,
            session: true
        }
    }
});
