/**
 * Created by betse on 9/28/15.
 */
Ext.define('PH.view.chargeableservice.ChargeableServiceGrid',{
    extend: 'CM.base.BaseGrid',
    alias: 'widget.chargeable-service-grid',
    requires: [
        'PH.view.chargeableservice.ChargeableServiceModel',
        'PH.view.chargeableservice.ChargeableServiceController'
    ],
    controller: 'chargeable-service',
    viewModel:{
        type: 'chargeable-service'
    },
    session: true,
    bind: '{chargeableServices}',
    columns:[
        {
            text: 'Code',
            dataIndex: 'code',
            flex: 1
        },
        {
            text: 'Name',
            dataIndex: 'name',
            flex: 3
        },
        {
            text: 'Order',
            dataIndex: 'order',
            flex: 1
        }
    ]
});