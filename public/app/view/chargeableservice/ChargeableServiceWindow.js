/**
 * Created by betse on 9/28/15.
 */
Ext.define('PH.view.chargeableservice.ChargeableServiceWindow',{
    extend: 'CM.base.BaseWindow',
    alias: 'widget.chargeable-service-window',
    width: 400,
    height: 320,
    bind:'{title}',
    session: true,
    items:[
        {
            xtype: 'form',
            reference: 'chargeableServiceForm',
            frame: false,
            bodyPadding: 15,
            items:[
                {
                    xtype: 'fieldset',
                    title: 'Chargeable Service Detail',
                    layout: 'vbox',
                    fieldDefaults:{
                        labelAlign: 'top',
                        labelWidth: 100,
                        labelStyle: 'font-weight:bold',
                        margin: '5 8 5 5',
                        width: '100%'
                    },
                    items:[
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Code',
                            name: 'code',
                            allowBlank: false,
                            bind: '{theChargeableService.code}'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Name',
                            name: 'name',
                            allowBlank: false,
                            bind: '{theChargeableService.name}'
                        },
                        {
                            xtype: 'numberfield',
                            fieldLabel: 'Order',
                            name: 'order',
                            allowBlank: false,
                            bind: '{theChargeableService.order}'
                        }
                    ]
                }
            ]
        }
    ],
    listeners:{
        close: 'onCancelButtonClick'
    },
    dockedItems: [
        {xtype: 'savecanceltoolbar'}
    ]
});
