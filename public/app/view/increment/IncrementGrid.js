/**
 * Created by betse on 9/30/15.
 */
Ext.define('PH.view.increment.IncrementGrid', {
  extend: 'CM.base.BaseGrid',
  alias: 'widget.increment-grid',
  requires: [
    'PH.view.increment.IncrementModel',
    'PH.view.increment.IncrementController'
  ],
  controller: 'increment',
  viewModel: {
    type: 'increment'
  },
  session: true,
  bind: '{increments}',
  reference: 'incrementGrid',
  columns: [
    {
      text: 'Code',
      dataIndex: 'code',
      flex: 1
    },
    {
      text: 'Name',
      dataIndex: 'name',
      flex: 2
    },
    {
      xtype: 'booleancolumn',
      text: 'Has Increment',
      dataIndex: 'has_increment',
      flex: 1,
      trueText: 'Yes',
      falseText: 'No'
    },
    {
      xtype: 'widgetcolumn',
      widget: {
        xtype: 'button',
        tooltip: 'see rates',
        text: 'Rates',
        handler: 'onRateButtonClick'
      }
    },
    {
      xtype: 'statebuttonwidget',
      flex: 1,
      widget: {
        xtype: 'button',
        tooltip: 'see increments',
        text: 'Additions',
        handler: 'onAdditionButtonClick'
      },
      onUpdate: function (record) {
        var has_increment = record.get('has_increment');
        this.setDisabled(!has_increment);
      }
    }
  ]
});
