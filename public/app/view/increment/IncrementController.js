/**
 * Created by betse on 9/30/15.
 */
Ext.define('PH.view.increment.IncrementController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.increment',
  requires: [
    'PH.view.increment.IncrementWindow',
    'PH.view.incrementrate.IncrementRateView',
    'PH.view.addition.AdditionView',
    'CM.util.Util'
  ],

  createDialog: function (record) {
    var me = this,
        view = me.getView();
    this.dialog = view.add({
      xtype: 'increment-window',
      viewModel: {
        data: {
          title: record ? 'Edit ' + record.get('name') : 'Add increment'
        },
        links: {
          theIncrement: record || {
            type: 'PH.model.increment.Increment',
            create: true
          }
        }
      }
    })
    view.up().up().mask();
    this.dialog.show();
  },
  onAddButtonClick: function () {
    "use strict";
    this.createDialog(null)
  },
  onEditButtonClick: function () {
    "use strict";
    var me = this,
        grid = me.getView(),
        selection = grid.getSelectionModel().getSelection();

    if (selection && selection.length > 0) {
      if (selection.length > 1) {
        CM.util.Util.showInfoMsg('Please select only one item to edit');
      } else {
        this.createDialog(selection[0]);
      }
    } else {
      CM.util.Util.showInfoMsg('You have to select a record to edit!');
    }
  },

  onSaveButtonClick: function () {
    "use strict";
    var me = this,
        form = me.lookupReference('incrementForm'),
        url,
        method,
        increment,
        data;
    if (form.isValid()) {
      increment = this.getViewModel().get("theIncrement").getData();
      data = {'service_increment': increment};
      if (this.getViewModel().get("theIncrement").phantom == true) {
        url = '/service_increments';
        method = 'POST'
      }
      else {
        url = '/service_increments/' + increment.id;
        method = 'PUT'
      }
      Ext.Ajax.request({
        method: method,
        url: url,
        headers: {'Content-Type': 'application/json'},
        jsonData: data,
        success: function (response) {
          var result = CM.util.Util.decodeJSON(response.responseText);
          if (result.success) {
            me.getStore('increments').load();
            me.onCancelButtonClick();
            CM.util.Util.showToast(result.message);
          } else {
            CM.util.Util.showErrorMsg(result.message);
          }
        },
        failure: function (response) {
          var result = CM.util.Util.decodeJSON(response.responseText);
          CM.util.Util.showErrorMsg(result.message);
        }
      });
    }
  },

  onRateButtonClick: function (btn) {
    "use strict";
    var record = btn.getWidgetRecord(),
        applicableIncrementRatesStore,
        serviceIncrementId = record.get('id'),
        win = Ext.create('PH.view.incrementrate.IncrementRateView', {title: record.get('name') + ' Rate'});
    applicableIncrementRatesStore = win.getViewModel().get('applicableIncrementRates');
    applicableIncrementRatesStore.load({
      url: '/service_increments/' + serviceIncrementId + '/increment_rates'
    });
    win.lookupReference('service_increment_id').setValue(serviceIncrementId);
    win.show();
  },

  onAdditionButtonClick: function (btn) {
    "use strict";
    var record = btn.getWidgetRecord(),
        applicableIncrementsOnIncrementsStore,
        additionsStore,
        serviceIncrementId = record.get('id'),
        win = Ext.create('PH.view.addition.AdditionView', {title: record.get('name') + ' Increments'});
    applicableIncrementsOnIncrementsStore = win.getViewModel().get('applicableIncrementsOnIncrements');
    additionsStore = win.getViewModel().get('additions');
    applicableIncrementsOnIncrementsStore.load({
      url: '/service_increments/' + serviceIncrementId + '/increments'
    });
    additionsStore.load({
      url: '/service_increments/' + serviceIncrementId + '/unassigned_increments'
    });
    win.lookupReference('service_increment_id_1').setValue(serviceIncrementId);
    win.show();
  },

  onCancelButtonClick: function () {
    "use strict";
    var view = this.getView();
    view.up().up().getEl().unmask();
    this.dialog = Ext.destroy(this.dialog);
  }
});
