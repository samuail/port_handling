/**
 * Created by betse on 9/30/15.
 */
Ext.define('PH.view.increment.IncrementWindow',{
    extend: 'CM.base.BaseWindow',
    alias: 'widget.increment-window',
    width: 400,
    height: 300,
    bind:'{title}',
    session: true,
    items:[
        {
            xtype: 'form',
            reference: 'incrementForm',
            frame: false,
            bodyPadding: 15,
            items:[
                {
                    xtype: 'fieldset',
                    title: 'Increment Detail',
                    layout: 'vbox',
                    fieldDefaults:{
                        labelAlign: 'top',
                        labelWidth: 100,
                        labelStyle: 'font-weight:bold',
                        margin: '5 8 5 5',
                        width: '100%'
                    },
                    items:[
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Code',
                            name: 'code',
                            allowBlank: false,
                            bind: '{theIncrement.code}'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Name',
                            name: 'name',
                            allowBlank: false,
                            bind: '{theIncrement.name}'
                        },
                        {
                            xtype: 'checkboxfield',
                            fieldLabel: 'Has Increment',
                            name: 'has_increment',
                            bind: '{theIncrement.has_increment}'
                        }
                    ]
                }
            ]
        }
    ],
    listeners:{
        close: 'onCancelButtonClick'
    },
    dockedItems: [
        {xtype: 'savecanceltoolbar'}
    ]
});
