/**
 * Created by betse on 9/30/15.
 */
Ext.define('PH.view.increment.IncrementModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.increment',
    requires: ['PH.model.increment.Increment'],

    stores:{
        increments:{
            model: 'PH.model.increment.Increment',
            autoLoad: true,
            session: true
        }
    }
});
