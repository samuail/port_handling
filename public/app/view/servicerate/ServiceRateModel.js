/**
 * Created by betse on 10/1/15.
 */
Ext.define('PH.view.servicerate.ServiceRateModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.service-rate',
    requires: [
        'PH.model.servicerate.ServiceRate',
        'PH.model.lookup.LookUp'
    ],

    stores:{
        serviceRates:{
            model: 'PH.model.servicerate.ServiceRate',
            autoLoad: false
        },
        containerTypes:{
            model: 'PH.model.lookup.LookUp',
            autoLoad: false
        },
        containerSizes:{
            model: 'PH.model.lookup.LookUp',
            autoLoad: false
        },
        transactionTypes:{
            model: 'PH.model.lookup.LookUp',
            autoLoad: false
        },
        ports:{
            model: 'PH.model.lookup.LookUp',
            autoLoad: false
        }
    }
});
