/**
 * Created by betse on 10/1/15.
 */
Ext.define('PH.view.servicerate.ServiceRateGrid',{
    extend: 'CM.base.BaseGridWithSave',
    alias: 'widget.service-rate-grid',
    requires:[
        'Ext.selection.CellModel',
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.util.*',
        'Ext.form.*'
    ],
    bind: '{serviceRates}',
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
    selModel: {
        selType: 'cellmodel'
    },
    height: 400,
    session: true,
    columns:[
        {
            text: 'Container Type',
            dataIndex: 'container_type_name',
            flex: 1,
            editor:{
                xtype: 'combo',
                displayField: 'name',
                valueField: 'name',
                queryMode: 'local',
                forceSelection: true,
                allowBlank: false,
                bind:{
                    store: '{containerTypes}'
                },
                listeners:{
                    change: 'onContainerTypeChange'
                }
            }
        },
        {
            text: 'Container Size',
            dataIndex: 'container_size_name',
            flex: 1,
            editor:{
                xtype: 'combo',
                displayField: 'name',
                valueField: 'name',
                queryMode: 'local',
                forceSelection: true,
                allowBlank: false,
                bind:{
                    store: '{containerSizes}'
                },
                listeners:{
                    change: 'onContainerSizeChange'
                }
            }
        },
        {
            text: 'Transaction Type',
            dataIndex: 'transaction_type_name',
            flex: 1,
            editor:{
                xtype: 'combo',
                displayField: 'name',
                valueField: 'name',
                queryMode: 'local',
                forceSelection: true,
                allowBlank: false,
                bind:{
                    store: '{transactionTypes}'
                },
                listeners:{
                    change: 'onTransactionTypeChange'
                }
            }
        },
        {
            text: 'ServiceDeliveryUnit',
            dataIndex: 'port_name',
            flex: 1,
            editor:{
                xtype: 'combo',
                displayField: 'name',
                valueField: 'name',
                queryMode: 'local',
                forceSelection: true,
                allowBlank: false,
                bind:{
                    store: '{service_delivery_units}'
                },
                listeners:{
                    change: 'onPortChange'
                }
            }
        },
        {
            xtype: 'widgetcolumn',
            widget:{
                xtype: 'button',
                tooltip: 'see rates',
                text: 'Prices',
                handler: 'onRateButtonClick'
            }
        }
    ]
});
