/**
 * Created by betse on 10/1/15.
 */
Ext.define('PH.view.servicerate.ServiceRateController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.service-rate',
    requires:[
        'PH.model.servicerate.ServiceRate',
        'CM.util.Util'
    ],
    onAddButtonClick: function(){
        "use strict";
        var rec = new PH.model.servicerate.ServiceRate();
        this.getViewModel().getStore("serviceRates").insert(0, rec);
    },

    onContainerTypeChange: function(combo, newValue, oldValue, eOpts){
        "use strict";
        this.lookupReference('serviceRateGrid').getSelectionModel().getSelection()[0].data.container_type_id =
            combo.lastSelection[0].data.id;
    },
    onContainerSizeChange: function(combo, newValue, oldValue, eOpts){
        "use strict";
        this.lookupReference('serviceRateGrid').getSelectionModel().getSelection()[0].data.container_size_id =
            combo.lastSelection[0].data.id;
    },
    onTransactionTypeChange: function(combo, newValue, oldValue, eOpts){
        "use strict";
        this.lookupReference('serviceRateGrid').getSelectionModel().getSelection()[0].data.transaction_type_id =
            combo.lastSelection[0].data.id;
    },
    onUnitOfChargeChange: function(combo, newValue, oldValue, eOpts){
        "use strict";
        this.lookupReference('serviceRateGrid').getSelectionModel().getSelection()[0].data.unit_of_charge_id =
            combo.lastSelection[0].data.id;
    },
    onPortChange: function(combo, newValue, oldValue, eOpts){
        "use strict";
        this.lookupReference('serviceRateGrid').getSelectionModel().getSelection()[0].data.port_id =
            combo.lastSelection[0].data.id;
    },

    onSaveToDBButtonClick: function () {
        "use strict";
        var me = this,
            newRecords = [],
            updatedRecords = [],
            successInd = 0,
            chargeableServiceId = this.lookupReference('chargeable_service_id').getValue(),
            store = this.getViewModel().getStore("serviceRates").getModifiedRecords();
        if(me.validate() === undefined) {
            for (var i = 0; i < store.length; i++) {
                if (store[i].phantom == false) {
                    updatedRecords.push(store[i].data);
                }
                else {
                    newRecords.push(store[i].data);
                }
            }
            if(updatedRecords.length > 0){
                for( var i=0; i < updatedRecords.length; i++){
                    Ext.Ajax.request({
                        method: 'PUT',
                        url: '/chargeable_services/'+ chargeableServiceId + '/service_rates/'+ updatedRecords[i].id,
                        headers: {'Content-Type': 'application/json'},
                        jsonData: updatedRecords[i],
                        success: function (response) {
                            var result = CM.util.Util.decodeJSON(response.responseText);
                            if (result.success) {
                                me.getStore('serviceRates').load({
                                    url: '/chargeable_services/'+ chargeableServiceId +'/service_rates'
                                });
                                CM.util.Util.showToast(result.message);
                            } else {
                                CM.util.Util.showErrorMsg(result.message);
                            }
                        },
                        failure: function (response) {
                            successInd = 1;
                            var result = CM.util.Util.decodeJSON(response.responseText);
                            CM.util.Util.showErrorMsg(result.message);
                        }
                    });
                }
            }
            if(newRecords.length > 0){
                for( var i=0; i < newRecords.length; i++){
                    Ext.Ajax.request({
                        method: 'POST',
                        url: '/chargeable_services/'+ chargeableServiceId +'/service_rates',
                        headers: {'Content-Type': 'application/json'},
                        jsonData: newRecords[i],
                        success: function (response) {
                            var result = CM.util.Util.decodeJSON(response.responseText);
                            if (result.success) {
                                me.getStore('serviceRates').load({
                                    url: '/chargeable_services/'+ chargeableServiceId +'/service_rates'
                                });
                                CM.util.Util.showToast(result.message);
                            } else {
                                CM.util.Util.showErrorMsg(result.message);
                            }
                        },
                        failure: function (response) {
                            successInd = 1;
                            var result = CM.util.Util.decodeJSON(response.responseText);
                            CM.util.Util.showErrorMsg(result.message);
                        }
                    });
                }
            }
        }
    },

    getColumnIndexes : function() {
        var me = this.lookupReference('serviceRateGrid'),
            columnIndexes = [];
        Ext.Array.each(me.columns, function (column) {
            if (Ext.isDefined(column.getEditor())) {
                columnIndexes.push(column.dataIndex);
            } else {
                columnIndexes.push(undefined);
            }
        });
        return columnIndexes;
    },

    validateRow :function(record, rowIndex){
        var me = this,
            view = this.lookupReference('serviceRateGrid'),
            errors = record.validate();
        if (errors.isValid()) {
            return true;
        }
        var columnIndexes = me.getColumnIndexes();
        Ext.each(columnIndexes, function (columnIndex, col) {
            var cellErrors, cell, messages;
            cellErrors = errors.getByField(columnIndex);
            if (!Ext.isEmpty(cellErrors)) {
                cell = view.getView().getCellByPosition({
                    row: rowIndex, column: col
                });
                messages = [];
                Ext.each(cellErrors, function (cellError) {
                    messages.push(cellError.message);
                });
                cell.addCls('x-form-error-msg x-form-invalid-icon x-form-invalid-icon-default');
                cell.set({
                    'data-errorqtip': Ext.String.format('<ul><li class="last">{0}</li></ul>',messages.join('<br/>'))
                });
            }
        });
        return false;
    },

    validate : function(){
        var me = this,
            isValid = true,
            view = this.lookupReference('serviceRateGrid'),
            error,
            record;
        Ext.each(view.getStore().getRange(), function (row, col) {
            //record = view.getRecord(row);
            isValid = (me.validateRow(row, col) && isValid);
        });
        error = isValid ? undefined : {
            title: "Invalid Records",
            message: "Please fix errors before saving."
        };
        return error;
    },

    onCancelButtonClick: function(){
        "use strict";
        var view = this.getView();
        view.getEl().unmask();
        this.dialog = Ext.destroy(this.dialog);
    }
});
