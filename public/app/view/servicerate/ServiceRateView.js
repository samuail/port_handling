/**
 * Created by betse on 10/1/15.
 */
Ext.define('PH.view.servicerate.ServiceRateView',{
    extend: 'Ext.window.Window',
    requires: [
        'PH.view.servicerate.ServiceRateController',
        'PH.view.servicerate.ServiceRateModel',
        'PH.view.servicerate.ServiceRateGrid'
    ],
    controller: 'service-rate',
    viewModel:{
        type: 'service-rate'
    },
    session: true,
    width: '70%',
    height: 450,
    modal: true,
    items:[
        {
            xtype: 'hiddenfield',
            reference: 'chargeable_service_id'
        },
        {
            xtype: 'service-rate-grid',
            flex: 1,
            reference: 'serviceRateGrid'
        }
    ]
});
