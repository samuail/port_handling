/**
 * Created by betse on 12/8/15.
 */
Ext.define('PH.view.csuoc.ChargeableServiceUnitOfChargeModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.chargeable-service-unit-of-charge',
    requires:[
        'PH.model.lookup.LookUp',
        'PH.model.csuoc.ChargeableServiceUnitOfCharge'
    ],

    stores:{
        unitOfCharges:{
            model: 'PH.model.lookup.LookUp',
            autoLoad: true,
            proxy:{
                type: 'baseproxy',
                url: '/unit_of_charges'
            }
        },
        chargeableServiceUnitOfCharges:{
            model: 'PH.model.csuoc.ChargeableServiceUnitOfCharge',
            autoLoad: true
        }
    }
});
