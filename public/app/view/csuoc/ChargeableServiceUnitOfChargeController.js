/**
 * Created by betse on 12/8/15.
 */
Ext.define('PH.view.csuoc.ChargeableServiceUnitOfChargeController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.chargeable-service-unit-of-charge',
    requires: [
        'CM.util.Util',
        'PH.view.assignment.AssignmentView'
    ],

    onGenerateButtonClick: function(){
        "use strict";
        var me = this;
        Ext.Ajax.request({
            method: 'POST',
            url: '/chargeable_service_unit_of_charges',
            success: function (response) {
                var result = CM.util.Util.decodeJSON(response.responseText);
                if (result.success) {
                    me.getViewModel().getStore('chargeableServiceUnitOfCharges').load();
                    CM.util.Util.showToast(result.message);
                } else {
                    CM.util.Util.showErrorMsg(result.message);
                }
            },
            failure: function (response) {
                var result = CM.util.Util.decodeJSON(response.responseText);
                CM.util.Util.showErrorMsg(result.message);
            }
        });
    },

    onUnitOfChargeChange: function(combo, newValue, oldValue, eOpts){
        "use strict";
        var me = this,
            grid = me.lookupReference('chargeableServiceUnitOfChargesGrid');
        grid.getSelectionModel().getSelection()[0].data.unit_of_charge_id =  combo.lastSelection[0].data.id;
    },

    onUpdateButtonClick: function(){
        "use strict";
        var me = this,
            modifiedRecords =[];
        if(me.getViewModel().getStore('chargeableServiceUnitOfCharges').getModifiedRecords().length > 0){
            me.getViewModel().getStore('chargeableServiceUnitOfCharges').each(function (rec) {
                if (rec.dirty == true)
                    modifiedRecords.push(rec.data);
            });
            Ext.Ajax.request({
                method: 'POST',
                url: 'chargeable_service_unit_of_charges/update_changes',
                headers: {'Content-Type': 'application/json'},
                jsonData: {
                    updated_unit_of_charges: modifiedRecords
                },
                success: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    if (result.success) {
                        CM.util.Util.showToast(result.message);
                        me.getViewModel().getStore('chargeableServiceUnitOfCharges').load();
                    } else {
                        CM.util.Util.showErrorMsg(result.message);
                    }
                },
                failure: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    CM.util.Util.showErrorMsg(result.message);
                }
            });
        }else{
            CM.util.Util.showErrorMsg('There is no modified record!');
        }
    },

    onIncrementButtonClick: function(btn){
        "use strict";
        var record = btn.getWidgetRecord(),
            applicableIncrementStore,
            incrementsStore,
            chargeableServiceUnitOfChargeId = record.get('id'),
            win = Ext.create('PH.view.assignment.AssignmentView', {title: record.get('service_delivery_unit_name') + ' Increments'});
        applicableIncrementStore = win.getViewModel().get('applicableIncrements');
        incrementsStore = win.getViewModel().get('increments');
        applicableIncrementStore.load({
            url: '/chargeable_service_unit_of_charges/'+ chargeableServiceUnitOfChargeId +'/increments'
        });
        incrementsStore.load({
            url: '/chargeable_service_unit_of_charges/'+ chargeableServiceUnitOfChargeId +'/unassigned_increments'
        });
        win.lookupReference('chargeable_service_unit_of_charge_id').setValue(chargeableServiceUnitOfChargeId);
        win.show();
    }
});
