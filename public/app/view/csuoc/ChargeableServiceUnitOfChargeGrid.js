/**
 * Created by betse on 12/8/15.
 */
Ext.define('PH.view.csuoc.ChargeableServiceUnitOfChargeGrid',{
    extend: 'Ext.container.Container',
    alias: 'widget.chargeable-service-unit-of-charge-grid',
    requires: [
        'PH.view.csuoc.ChargeableServiceUnitOfChargeModel',
        'PH.view.csuoc.ChargeableServiceUnitOfChargeController',
        'Ext.selection.CellModel',
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.util.*',
        'Ext.form.*'
    ],
    controller: 'chargeable-service-unit-of-charge',
    viewModel:{
        type: 'chargeable-service-unit-of-charge'
    },
    session: true,
    layout: 'vbox',
    width: '100%',
    items:[
        {
            xtype: 'fieldset',
            title: 'controls',
            margin: 5,
            layout: 'hbox',
            width: '100%',
            items: [
                {
                    xtype: 'button',
                    text: 'Generate',
                    handler: 'onGenerateButtonClick',
                    margin: 5
                },
                {
                    xtype: 'button',
                    text: 'Update',
                    handler: 'onUpdateButtonClick',
                    margin: 5
                }
            ]
        },
        {
            xtype: 'grid',
            width: '100%',
            reference: 'chargeableServiceUnitOfChargesGrid',
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 1
                })
            ],
            selModel: {
                selType: 'cellmodel'
            },
            bind: '{chargeableServiceUnitOfCharges}',
            columns:[
                {
                    text: 'Service Delivery Unit',
                    dataIndex: 'service_delivery_unit_name',
                    flex: 2
                },
                {
                    text: 'Chargeable Service Name',
                    dataIndex: 'chargeable_service_name',
                    flex: 2
                },
                {
                    text: 'Unit of Charge',
                    dataIndex: 'unit_of_charge_name',
                    flex: 1,
                    editor:{
                        xtype: 'combo',
                        valueField: 'name',
                        displayField: 'name',
                        forceSelection: true,
                        allowBlank: false,
                        queryMode: 'local',
                        bind:{
                            store: '{unitOfCharges}'
                        },
                        listeners:{
                            change: 'onUnitOfChargeChange'
                        }
                    }
                },
                {
                    xtype: 'checkcolumn',
                    header: 'Has Increment ?',
                    dataIndex: 'has_increment',
                    flex: 1,
                    stopSelection: false
                },
                {
                    xtype: 'statebuttonwidget',
                    flex: 1,
                    widget:{
                        xtype: 'button',
                        tooltip: 'see increments',
                        text: 'Increments',
                        handler: 'onIncrementButtonClick'
                    },
                    onUpdate: function(record) {
                        var has_increment = record.get('has_increment');
                        this.setDisabled(!has_increment);
                    }
                }
            ]
        }
    ]
});
