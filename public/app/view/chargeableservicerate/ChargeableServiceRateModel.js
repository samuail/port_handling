/**
 * Created by betse on 10/14/15.
 */
Ext.define('PH.view.chargeableservicerate.ChargeableServiceRateModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.chargeable-service-rate',
    requires:[
        'PH.model.servicerate.ServiceRate',
        'PH.model.servicedeliveryunit.ServiceDeliveryUnit',
        'PH.model.chargeableservice.ChargeableService'
    ],

    stores:{
        chargeableServiceRates:{
            model: 'PH.model.servicerate.ServiceRate',
            autoLoad: true
        },
        serviceDeliveryUnits:{
            model: 'PH.model.servicedeliveryunit.ServiceDeliveryUnit',
            autoLoad: true
        },
        chargeableServices:{
            model: 'PH.model.chargeableservice.ChargeableService',
            autoLoad: false
        }
    }
});
