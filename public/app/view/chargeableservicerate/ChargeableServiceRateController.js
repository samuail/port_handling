/**
 * Created by betse on 10/14/15.
 */
Ext.define('PH.view.chargeableservicerate.ChargeableServiceRateController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.chargeable-service-rate',
    requires: [
        'CM.util.Util'
    ],

    onGenerateButtonClick: function(){
        "use strict";
        var me = this,
            effectiveDate;
        effectiveDate = me.lookupReference('effectiveDate').value
        if(effectiveDate == null)
            CM.util.Util.showErrorMsg("Please enter effective date")
        else{
            Ext.Ajax.request({
                method: 'POST',
                url: '/service_rates',
                headers: { 'Content-Type': 'application/json' },
                jsonData:{
                    effective_date : effectiveDate
                },
                success: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    if (result.success) {
                        me.getStore('chargeableServiceRates').load();
                        CM.util.Util.showToast(result.message);
                        me.lookupReference('effectiveDate').setValue("");
                    } else {
                        CM.util.Util.showErrorMsg(result.message);
                    }
                },
                failure: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    CM.util.Util.showErrorMsg(result.message);
                }
            });
        }
    },

    /*onAfterRender: function(){
        "use strict";
        this.getViewModel().getStore('ports').load({
            url: '/ports'
        });
    },*/

    onServiceDeliveryUnitChange: function(combo, newValue, oldValue, eOpts){
        "use strict";
        this.getViewModel().getStore('chargeableServices').load({
            url: '/service_rates/'+0+'/get_chargeable_services_for_service_delivery_unit/'+newValue
        });
    },

    onChargeableServiceChange: function(combo, newValue, oldValue, eOpts){
        "use strict";
        var me = this,
            portId;
        portId = me.lookupReference('portsCombo').lastValue;
        this.getViewModel().getStore('chargeableServiceRates').load({
            url: '/service_rates/'+portId+'/get_rate_for_chargeable_service/'+newValue
        });
    },

    onListAllButtonClick: function(){
        "use strict";
        this.getViewModel().getStore('chargeableServiceRates').load()
    },

    onUpdateButtonClick: function(){
        "use strict";
        var me = this,
            modifiedRecords =[],
            ratesStore,
            effectiveDate;
        effectiveDate = me.lookupReference('effectiveDate').value
        if(effectiveDate == null)
            CM.util.Util.showErrorMsg("Please enter effective date");
        else {
            ratesStore = me.getViewModel().getStore('chargeableServiceRates').getModifiedRecords();
            for (var i = 0; i < ratesStore.length; i++) {
                modifiedRecords.push(ratesStore[i].data);
            }
            if(modifiedRecords.length > 0){
                Ext.Ajax.request({
                    method: 'PUT',
                    url: '/service_rates/' + ratesStore[0].data.id,
                    headers: {'Content-Type': 'application/json'},
                    jsonData: {
                        updated_service_rates: modifiedRecords
                    },
                    success: function (response) {
                        var result = CM.util.Util.decodeJSON(response.responseText);
                        if (result.success) {
                            me.getStore('chargeableServiceRates').load();
                            CM.util.Util.showToast(result.message);
                        } else {
                            CM.util.Util.showErrorMsg(result.message);
                        }
                    },
                    failure: function (response) {
                        var result = CM.util.Util.decodeJSON(response.responseText);
                        CM.util.Util.showErrorMsg(result.message);
                    }
                });
            }else{
                CM.util.Util.showInfoMsg('There is no updated rate');
            }
        }
    }
});
