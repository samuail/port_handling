/**
 * Created by betse on 10/6/15.
 */
Ext.define('PH.view.incrementrate.IncrementRateGrid',{
    extend: 'CM.base.BaseGrid',
    alias: 'widget.increment-rate-grid',
    session: true,
    bind: '{applicableIncrementRates}',
    height: 400,
    columns:[
        {
            text: 'Percent',
            dataIndex: 'percent',
            flex: 1
        },
        {
            text: 'Effective Date',
            dataIndex: 'effective_date',
            flex: 1
        }
    ]
});
