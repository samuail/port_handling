/**
 * Created by betse on 10/6/15.
 */
Ext.define('PH.view.incrementrate.IncrementRateView',{
    extend: 'Ext.window.Window',
    requires: [
        'PH.view.incrementrate.IncrementRateController',
        'PH.view.incrementrate.IncrementRateModel',
        'PH.view.incrementrate.IncrementRateGrid'
    ],
    controller: 'increment-rate',
    viewModel:{
        type: 'increment-rate'
    },
    session: true,
    width: '50%',
    height: 450,
    modal: true,
    items:[
        {
            xtype: 'hiddenfield',
            reference: 'service_increment_id'
        },
        {
            xtype: 'increment-rate-grid',
            flex: 1,
            reference: 'incrementRateGrid'
        }
    ]
});
