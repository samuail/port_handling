/**
 * Created by betse on 10/6/15.
 */
Ext.define('PH.view.incrementrate.IncrementRateWindow',{
    extend: 'CM.base.BaseWindow',
    alias: 'widget.increment-rate-window',
    width: 400,
    height: 250,
    bind:'{title}',
    session: true,
    items:[
        {
            xtype: 'form',
            reference: 'incrementRateForm',
            frame: false,
            bodyPadding: 15,
            items:[
                {
                    xtype: 'fieldset',
                    title: 'Increment Detail',
                    layout: 'vbox',
                    fieldDefaults:{
                        labelAlign: 'top',
                        labelWidth: 100,
                        labelStyle: 'font-weight:bold',
                        margin: '5 8 5 5',
                        width: '100%'
                    },
                    items:[
                        {
                            xtype: 'numberfield',
                            fieldLabel: 'percent',
                            name: 'percent',
                            allowBlank: false,
                            bind: '{theIncrementRate.percent}'
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Effective Date',
                            name: 'effective_date',
                            allowBlank: false,
                            bind: '{theIncrementRate.effective_date}'
                        }
                    ]
                }
            ]
        }
    ],
    listeners:{
        close: 'onCancelButtonClick'
    },
    dockedItems: [
        {xtype: 'savecanceltoolbar'}
    ]
});
