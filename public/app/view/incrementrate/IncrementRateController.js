/**
 * Created by betse on 10/6/15.
 */
Ext.define('PH.view.incrementrate.IncrementRateController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.increment-rate',
  requires: [
    'CM.util.Util',
    'PH.view.incrementrate.IncrementRateWindow'
  ],
  createDialog: function (record) {
    var me = this,
        view = me.getView();
    this.dialog = view.add({
      xtype: 'increment-rate-window',
      viewModel: {
        data: {
          title: record ? 'Edit increment rate' : 'Add increment rate'
        },
        links: {
          theIncrementRate: record || {
            type: 'PH.model.incrementrate.IncrementRate',
            create: true
          }
        }
      }
    })
    view.mask();
    this.dialog.show();
  },
  onAddButtonClick: function () {
    "use strict";
    this.createDialog(null)
  },
  onEditButtonClick: function () {
    "use strict";
    var me = this,
        grid = me.lookupReference('incrementRateGrid'),
        selection = grid.getSelectionModel().getSelection();

    if (selection && selection.length > 0) {
      if (selection.length > 1) {
        CM.util.Util.showInfoMsg('Please select only one item to edit');
      } else {
        this.createDialog(selection[0]);
      }
    } else {
      CM.util.Util.showInfoMsg('You have to select a record to edit!');
    }
  },

  onSaveButtonClick: function () {
    "use strict";
    var me = this,
        form = me.lookupReference('incrementRateForm'),
        url,
        method,
        serviceIncrementId,
        incrementRate,
        data;
    if (form.isValid()) {
      incrementRate = this.getViewModel().get("theIncrementRate").getData();
      data = {'increment_rate': incrementRate};
      serviceIncrementId = parseInt(this.lookupReference('service_increment_id').value);
      if (this.getViewModel().get("theIncrementRate").phantom == true) {
        url = '/service_increments/' + serviceIncrementId + '/increment_rates';
        method = 'POST'
      }
      else {
        url = '/service_increments/' + serviceIncrementId + '/increment_rates/' + incrementRate.id;
        method = 'PUT'
      }
      Ext.Ajax.request({
        method: method,
        url: url,
        headers: {'Content-Type': 'application/json'},
        jsonData: data,
        success: function (response) {
          var result = CM.util.Util.decodeJSON(response.responseText);
          if (result.success) {
            me.getStore('applicableIncrementRates').load({
              url: '/service_increments/' + serviceIncrementId + '/increment_rates'
            });
            me.onCancelButtonClick();
            CM.util.Util.showToast(result.message);
          } else {
            CM.util.Util.showErrorMsg(result.message);
          }
        },
        failure: function (response) {
          var result = CM.util.Util.decodeJSON(response.responseText);
          CM.util.Util.showErrorMsg(result.message);
        }
      });
    }
  },

  onCancelButtonClick: function () {
    "use strict";
    var view = this.getView();
    view.getEl().unmask();
    this.dialog = Ext.destroy(this.dialog);
  }
});
