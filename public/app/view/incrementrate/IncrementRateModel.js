/**
 * Created by betse on 10/6/15.
 */
Ext.define('PH.view.incrementrate.IncrementRateModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.increment-rate',
    requires: ['PH.model.incrementrate.IncrementRate'],

    stores: {
        applicableIncrementRates:{
            model: 'PH.model.incrementrate.IncrementRate',
            autoLoad: false,
            session: true
        }
    }
});
