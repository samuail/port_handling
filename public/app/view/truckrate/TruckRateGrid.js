/**
 * Created by betse on 10/30/15.
 */
Ext.define('PH.view.truckrate.TruckRateGrid',{
    extend: 'Ext.container.Container',
    alias: 'widget.truck-rate-grid',
    requires: [
        'PH.view.truckrate.TruckRateModel',
        'PH.view.truckrate.TruckRateController',
        'Ext.selection.CellModel',
        'Ext.grid.filters.Filters',
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.util.*',
        'Ext.form.*'
    ],
    controller: 'truck-rate',
    viewModel: {
        type: 'truck-rate'
    },
    width: '100%',
    layout: 'vbox',
    items:[
        {
            xtype: 'fieldset',
            title: 'Rate controls',
            layout: 'hbox',
            width: '100%',
            margin: '5 0 0 5',
            items:[
                {
                    xtype: 'datefield',
                    fieldLabel: 'Effective Date',
                    reference: 'effectiveDate',
                    margin: '5 30 5 5'
                },
                {
                    xtype: 'button',
                    text: 'Generate',
                    handler: 'onGenerateButtonClick',
                    margin: '5 30 5 5'
                },
                {
                    xtype: 'button',
                    text: 'Update',
                    handler: 'onUpdateButtonClick',
                    margin: '5 5 5 5'
                }
            ]
        },
        {
            xtype: 'grid',
            width: '100%',
            flex: 1,
            columnLines: true,
            plugins: [
                'gridfilters',
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 1
                })
            ],
            selModel: {
                selType: 'cellmodel'
            },
            bind: '{truckRates}',
            reference: 'truckRatesGrid',
            columns:[
                {
                    text: 'Service Delivery Unit',
                    dataIndex: 'service_delivery_unit_name',
                    flex: 1,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                {
                    text: 'Chargeable Service Name',
                    dataIndex: 'chargeable_service_name',
                    flex: 2,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                {
                    text: 'Transaction Type',
                    dataIndex: 'transaction_type_name',
                    flex: 1,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                {
                    text: 'Low Rate',
                    dataIndex: 'low',
                    flex: 1,
                    editor:{
                        xtype: 'numberfield',
                        allowBlank: false
                    }
                },
                {
                    text: 'Medium Rate',
                    dataIndex: 'medium',
                    flex: 1,
                    editor:{
                        xtype: 'numberfield',
                        allowBlank: false
                    }
                },
                {
                    text: 'High Rate',
                    dataIndex: 'high',
                    flex: 1,
                    editor:{
                        xtype: 'numberfield',
                        allowBlank: false
                    }
                },

                {
                    text: 'Effective Date',
                    dataIndex: 'effective_date',
                    flex: 1
                }
            ]
        }
    ]
});
