/**
 * Created by betse on 10/30/15.
 */
Ext.define('PH.view.truckrate.TruckRateModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.truck-rate',
    requires: [
        'PH.model.truckrate.TruckRate',
        'PH.model.servicedeliveryunit.ServiceDeliveryUnit'
    ],

    stores:{
        truckRates: {
            model: 'PH.model.truckrate.TruckRate',
            autoLoad: true
        }
    }
});
