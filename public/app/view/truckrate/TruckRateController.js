/**
 * Created by betse on 10/30/15.
 */
Ext.define('PH.view.truckrate.TruckRateController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.truck-rate',
    requires:[
        'CM.util.Util'
    ],

    onGenerateButtonClick: function(){
        "use strict";
        var me = this,
            effectiveDate;
        effectiveDate = me.lookupReference('effectiveDate').value;
        if(effectiveDate == null)
            CM.util.Util.showErrorMsg("Please enter effective date");
        else{
            Ext.Ajax.request({
                method: 'POST',
                url: '/truck_rates',
                headers: { 'Content-Type': 'application/json' },
                jsonData:{
                    effective_date : effectiveDate
                },
                success: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    if (result.success) {
                        me.getStore('truckRates').load();
                        CM.util.Util.showToast(result.message);
                        me.lookupReference('effectiveDate').setValue("");
                    } else {
                        CM.util.Util.showErrorMsg(result.message);
                    }
                },
                failure: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    CM.util.Util.showErrorMsg(result.message);
                }
            });
        }
    },

    onUpdateButtonClick: function(){
        "use strict";
        var me = this,
            modifiedRecords =[],
            ratesStore,
            effectiveDate;
        effectiveDate = me.lookupReference('effectiveDate').value;
        if(effectiveDate == null)
            CM.util.Util.showErrorMsg("Please enter effective date");
        else {
            ratesStore = me.getViewModel().getStore('truckRates').getModifiedRecords();
            for (var i = 0; i < ratesStore.length; i++) {
                modifiedRecords.push(ratesStore[i].data);
            }
            if(modifiedRecords.length > 0) {
                Ext.Ajax.request({
                    method: 'PUT',
                    url: '/truck_rates/' + ratesStore[0].data.id,
                    headers: {'Content-Type': 'application/json'},
                    jsonData: {
                        updated_service_rates: modifiedRecords
                    },
                    success: function (response) {
                        var result = CM.util.Util.decodeJSON(response.responseText);
                        if (result.success) {
                            me.getStore('truckRates').load();
                            CM.util.Util.showToast(result.message);
                        } else {
                            CM.util.Util.showErrorMsg(result.message);
                        }
                    },
                    failure: function (response) {
                        var result = CM.util.Util.decodeJSON(response.responseText);
                        CM.util.Util.showErrorMsg(result.message);
                    }
                });
            }else{
                CM.util.Util.showInfoMsg('There is no updated rate');
            }
        }
    }
});
