/**
 * Created by betse on 10/12/15.
 */
Ext.define('PH.view.addition.AdditionWindow',{
    extend: 'CM.base.BaseWindow',
    alias: 'widget.addition-window',
    width: 400,
    height: 200,
    bind:'{title}',
    session: true,
    items:[
        {
            xtype: 'hiddenfield',
            reference: 'service_increment_id_2'
        },
        {
            xtype: 'grid',
            reference: 'additionGridWindow',
            selType: 'checkboxmodel',
            bind: '{additions}',
            columnLines: true,
            columns: [
                {
                    text: "Name",
                    dataIndex: 'name',
                    flex: 1
                }
            ]
        }
    ],
    listeners:{
        close: 'onCancelButtonClick'
    },
    dockedItems: [
        {xtype: 'savecanceltoolbar'}
    ]
});
