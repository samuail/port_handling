/**
 * Created by betse on 10/12/15.
 */
Ext.define('PH.view.addition.AdditionModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.addition',
    requires: [
        'PH.model.increment.Increment'
    ],

    stores:{
        applicableIncrementsOnIncrements:{
            model: 'PH.model.increment.Increment',
            autoLoad: false
        },
        additions:{
            model: 'PH.model.increment.Increment',
            autoLoad: false
        }
    }
});
