/**
 * Created by betse on 10/12/15.
 */
Ext.define('PH.view.addition.AdditionGrid',{
    extend: 'CM.base.BaseGridWithSave',
    alias: 'widget.addition-grid',
    bind: '{applicableIncrementsOnIncrements}',
    height: 400,
    session: true,
    columns:[
        {
            text: 'Increments',
            dataIndex: 'name',
            flex: 1
        }
    ],
    listeners:{
        afterrender: 'onAfterRender'
    }
});
