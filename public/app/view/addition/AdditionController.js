/**
 * Created by betse on 10/12/15.
 */
Ext.define('PH.view.addition.AdditionController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.addition',
  requires: [
    'PH.view.addition.AdditionWindow',
    'CM.util.Util'
  ],

  createDialog: function (record) {
    var me = this,
        view = me.getView();
    this.dialog = view.add({
      xtype: 'addition-window',
      viewModel: {
        data: {
          title: 'Assign increment'
        }
      }
    });
    view.mask();
    this.dialog.show();
  },
  onAddButtonClick: function () {
    "use strict";
    this.createDialog(null);
  },

  onCancelButtonClick: function () {
    "use strict";
    var view = this.getView();
    view.getEl().unmask();
    this.dialog = Ext.destroy(this.dialog);
  },

  onSaveButtonClick: function () {
    "use strict";
    var me = this,
        grid = me.lookupReference('additionGridWindow'),
        serviceIncrementId,
        data;
    serviceIncrementId = parseInt(this.lookupReference('service_increment_id_1').value);
    for (var i = 0; i < grid.getSelectionModel().getSelection().length; i++) {
      data = {
        'addendum': {
          'service_increment_id': serviceIncrementId,
          'addition_id': grid.getSelectionModel().getSelection()[i].data.id
        }
      };
      Ext.Ajax.request({
        method: 'POST',
        url: '/service_increments/' + serviceIncrementId + '/addendums',
        headers: {'Content-Type': 'application/json'},
        jsonData: data,
        success: function (response) {
          var result = CM.util.Util.decodeJSON(response.responseText);
          if (result.success) {
            me.getStore('applicableIncrementsOnIncrements').load({
              url: '/service_increments/' + serviceIncrementId + '/increments'
            });
            me.getStore('additions').load({
              url: '/service_increments/' + serviceIncrementId + '/unassigned_increments'
            });
            me.onCancelButtonClick();
            CM.util.Util.showToast(result.message);
          } else {
            CM.util.Util.showErrorMsg(result.message);
          }
        },
        failure: function (response) {
          var result = CM.util.Util.decodeJSON(response.responseText);
          CM.util.Util.showErrorMsg(result.message);
        }
      });
    }
  },
  onAfterRender: function(){
    "use strict";
    Ext.ComponentQuery.query("#save")[0].hide();
  }
});
