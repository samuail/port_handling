/**
 * Created by betse on 10/12/15.
 */
Ext.define('PH.view.addition.AdditionView',{
    extend: 'Ext.window.Window',
    requires: [
        'PH.view.addition.AdditionController',
        'PH.view.addition.AdditionModel',
        'PH.view.addition.AdditionGrid'
    ],
    controller: 'addition',
    viewModel:{
        type: 'addition'
    },
    session: true,
    width: 800,
    height: 450,
    modal: true,
    items:[
        {
            xtype: 'hiddenfield',
            reference: 'service_increment_id_1'
        },
        {
            xtype: 'addition-grid',
            flex: 1,
            reference: 'additionGrid'
        }
    ]
});
