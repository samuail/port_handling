/**
 * Created by betse on 10/2/15.
 */
Ext.define('PH.view.assignment.AssignmentController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.assignment',
  requires: [
    'PH.view.assignment.AssignmentWindow',
    'CM.util.Util'
  ],

  createDialog: function (record) {
    var me = this,
        view = me.getView();
    this.dialog = view.add({
      xtype: 'assignment-window',
      viewModel: {
        data: {
          title: 'Assign increment'
        }
      }
    });
    view.mask();
    this.dialog.show();
  },
  onAddButtonClick: function () {
    "use strict";
    this.createDialog(null);
  },

  onCancelButtonClick: function () {
    "use strict";
    var view = this.getView();
    view.getEl().unmask();
    this.dialog = Ext.destroy(this.dialog);
  },

  onSaveButtonClick: function () {
    "use strict";
    var me = this,
        grid = me.lookupReference('assignmentGridWindow'),
        chargeableServiceUnitOfChargeId,
        data;
      chargeableServiceUnitOfChargeId = parseInt(this.lookupReference('chargeable_service_unit_of_charge_id').value);
    for (var i = 0; i < grid.getSelectionModel().getSelection().length; i++) {
      data = {
        'chargeable_service_increment': {
          'chargeable_service_unit_of_charge_id': chargeableServiceUnitOfChargeId,
          'service_increment_id': grid.getSelectionModel().getSelection()[i].data.id
        }
      };

      Ext.Ajax.request({
        method: 'POST',
        url: '/chargeable_service_increments',
        headers: {'Content-Type': 'application/json'},
        jsonData: data,
        success: function (response) {
          var result = CM.util.Util.decodeJSON(response.responseText);
          if (result.success) {
            me.getStore('applicableIncrements').load({
                url: '/chargeable_service_unit_of_charges/'+ chargeableServiceUnitOfChargeId +'/increments'
            });
            me.getStore('increments').load({
                url: '/chargeable_service_unit_of_charges/'+ chargeableServiceUnitOfChargeId +'/unassigned_increments'
            });
            me.onCancelButtonClick();
            CM.util.Util.showToast(result.message);
          } else {
            CM.util.Util.showErrorMsg(result.message);
          }
        },
        failure: function (response) {
          var result = CM.util.Util.decodeJSON(response.responseText);
          CM.util.Util.showErrorMsg(result.message);
        }
      });
    }
  },
  onAfterRender: function(){
    "use strict";
    Ext.ComponentQuery.query("#save")[0].hide();
  }
});
