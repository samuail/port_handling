/**
 * Created by betse on 10/2/15.
 */
Ext.define('PH.view.assignment.AssignmentWindow',{
    extend: 'CM.base.BaseWindow',
    alias: 'widget.assignment-window',
    //requires: ['Ext.grid.Panel'],
    width: 400,
    height: 200,
    bind:'{title}',
    session: true,
    items:[
        {
            xtype: 'hiddenfield',
            reference: 'chargeable_service_id_1'
        },
        {
            xtype: 'grid',
            reference: 'assignmentGridWindow',
            selType: 'checkboxmodel',
            bind: '{increments}',
            columnLines: true,
            columns: [
                {
                    text: "Name",
                    dataIndex: 'name',
                    flex: 1
                }
            ]
        }
    ],
    listeners:{
        close: 'onCancelButtonClick'
    },
    dockedItems: [
        {xtype: 'savecanceltoolbar'}
    ]
});
