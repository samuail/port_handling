/**
 * Created by betse on 10/2/15.
 */
Ext.define('PH.view.assignment.AssignmentView',{
    extend: 'Ext.window.Window',
    requires: [
        'PH.view.assignment.AssignmentController',
        'PH.view.assignment.AssignmentModel',
        'PH.view.assignment.AssignmentGrid'
    ],
    controller: 'assignment',
    viewModel:{
        type: 'assignment'
    },
    session: true,
    width: 800,
    height: 450,
    modal: true,
    items:[
        {
            xtype: 'hiddenfield',
            reference: 'chargeable_service_unit_of_charge_id'
        },
        {
            xtype: 'assignment-grid',
            flex: 1,
            reference: 'assignmentGrid'
        }
    ]
});
