/**
 * Created by betse on 10/2/15.
 */
Ext.define('PH.view.assignment.AssignmentModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.assignment',
    requires: ['PH.model.increment.Increment'],

    stores:{
        applicableIncrements:{
            model: 'PH.model.increment.Increment',
            autoLoad: false
        },

        increments:{
            model: 'PH.model.increment.Increment',
            autoLoad: false
        }
    }
});