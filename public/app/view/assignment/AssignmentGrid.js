/**
 * Created by betse on 10/2/15.
 */
Ext.define('PH.view.assignment.AssignmentGrid',{
    extend: 'CM.base.BaseGridWithSave',
    alias: 'widget.assignment-grid',
    bind: '{applicableIncrements}',
    height: 400,
    session: true,
    columns:[
        {
            text: 'Increments',
            dataIndex: 'name',
            flex: 1
        }
    ],
    listeners:{
        afterrender: 'onAfterRender'
    }
});
