/**
 * Created by betse on 10/20/15.
 */
Ext.define('PH.view.servicedeliveryunit.ServiceDeliveryUnitGrid',{
    extend: 'CM.base.BaseGrid',
    alias: 'widget.service-delivery-unit-grid',
    requires: [
        'PH.view.servicedeliveryunit.ServiceDeliveryUnitModel',
        'PH.view.servicedeliveryunit.ServiceDeliveryUnitController'
    ],
    controller: 'service-delivery-unit',
    viewModel:{
        type: 'service-delivery-unit'
    },
    session: true,
    bind: '{serviceDeliveryUnits}',
    columns:[
        {
            text: 'Code',
            dataIndex: 'code',
            flex: 1
        },
        {
            text: 'Name',
            dataIndex: 'name',
            flex: 1
        },
        {
            text: 'Address',
            dataIndex: 'address',
            flex:1
        },
        {
            text: 'Currency',
            dataIndex: 'currency_name',
            flex: 1
        },
        {
            text: 'Type',
            dataIndex: 'type_name',
            flex: 1
        },
        {
            xtype: 'widgetcolumn',
            widget:{
                xtype: 'button',
                tooltip: 'see applicable services',
                text: 'Services',
                handler: 'onServicesButtonClick'
            }
        }
    ]
});
