/**
 * Created by betse on 10/20/15.
 */
Ext.define('PH.view.servicedeliveryunit.ServiceDeliveryUnitWindow',{
    extend: 'CM.base.BaseWindow',
    alias: 'widget.service-delivery-unit-window',
    width: 400,
    height:460,
    bind:'{title}',
    session: true,
    items:[
        {
            xtype: 'form',
            reference: 'serviceDeliveryUnitForm',
            frame: false,
            bodyPadding: 15,
            items:[
                {
                    xtype: 'fieldset',
                    title: 'Service Delivery Unit Detail',
                    layout: 'vbox',
                    fieldDefaults:{
                        labelAlign: 'top',
                        labelWidth: 100,
                        labelStyle: 'font-weight:bold',
                        margin: '5 8 5 5',
                        width: '100%'
                    },
                    items:[
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Code',
                            name: 'code',
                            allowBlank: false,
                            bind: '{theServiceDeliveryUnit.code}'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Name',
                            name: 'name',
                            allowBlank: false,
                            bind: '{theServiceDeliveryUnit.name}'
                        },
                        {
                            xtype: 'textareafield',
                            fieldLabel: 'Address',
                            name: 'address',
                            allowBlank: true,
                            bind: '{theServiceDeliveryUnit.address}'
                        },
                        {
                            xtype: 'combo',
                            fieldLabel: 'Currency',
                            displayField: 'name',
                            valueField: 'id',
                            queryMode: 'local',
                            forceSelection: true,
                            allowBlank: false,
                            bind:{
                                value: '{theServiceDeliveryUnit.currency_id}',
                                store: '{currencies}'
                            }
                        },
                        {
                            xtype: 'combo',
                            fieldLabel: 'Type',
                            displayField: 'name',
                            valueField: 'id',
                            queryMode: 'local',
                            forceSelection: true,
                            allowBlank: false,
                            bind:{
                                value: '{theServiceDeliveryUnit.service_delivery_unit_type_id}',
                                store: '{serviceDeliveryTypes}'
                            }
                        }
                    ]
                }
            ]
        }
    ],
    listeners:{
        close: 'onCancelButtonClick'
    },
    dockedItems: [
        {xtype: 'savecanceltoolbar'}
    ]
});