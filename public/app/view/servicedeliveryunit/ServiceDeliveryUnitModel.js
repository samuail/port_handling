/**
 * Created by betse on 10/20/15.
 */
Ext.define('PH.view.servicedeliveryunit.ServiceDeliveryUnitModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.service-delivery-unit',
    requires: [
        'PH.model.servicedeliveryunit.ServiceDeliveryUnit',
        'PH.model.servicedeliveryunittype.ServiceDeliveryUnitType',
        'PH.model.currency.Currency'
    ],

    stores:{
        serviceDeliveryUnits:{
            model: 'PH.model.servicedeliveryunit.ServiceDeliveryUnit',
            autoLoad: true,
            session: true
        },
        currencies:{
            model: 'PH.model.currency.Currency',
            autoLoad: true
        },
        serviceDeliveryTypes:{
            model: 'PH.model.servicedeliveryunittype.ServiceDeliveryUnitType',
            autoLoad: true
        }
    }
});