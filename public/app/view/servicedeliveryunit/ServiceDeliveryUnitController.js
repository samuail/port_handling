/**
 * Created by betse on 10/20/15.
 */
Ext.define('PH.view.servicedeliveryunit.ServiceDeliveryUnitController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.service-delivery-unit',
    requires:[
        'PH.view.servicedeliveryunit.ServiceDeliveryUnitWindow',
        'PH.view.servicedeliveryunitservice.ServiceDeliveryUnitServiceView',
        'CM.util.Util'
    ],

    createDialog: function(record){
        var me = this,
            view = me.getView();
        this.dialog = view.add({
            xtype: 'service-delivery-unit-window',
            viewModel:{
                data: {
                    title: record ? 'Edit '+ record.get('name') : 'Add service delivery unit'
                },
                links:{
                    theServiceDeliveryUnit: record||{
                        type: 'PH.model.servicedeliveryunit.ServiceDeliveryUnit',
                        create: true
                    }
                }
            }
        })
        view.up().up().mask();
        this.dialog.show();
    },
    onAddButtonClick: function(){
        "use strict";
        this.createDialog(null);
    },
    onEditButtonClick: function () {
        "use strict";
        var me = this,
            grid = me.getView(),
            selection = grid.getSelectionModel().getSelection();

        if (selection && selection.length > 0) {
            if (selection.length > 1) {
                CM.util.Util.showInfoMsg('Please select only one item to edit');
            } else {
                this.createDialog(selection[0]);
            }
        } else {
            CM.util.Util.showInfoMsg('You have to select a record to edit!');
        }
    },

    onSaveButtonClick: function () {
        "use strict";
        var me = this,
            form = me.lookupReference('serviceDeliveryUnitForm'),
            url,
            method,
            serviceDeliveryUnit,
            data;
        if(form.isValid())
        {
            serviceDeliveryUnit = this.getViewModel().get("theServiceDeliveryUnit").getData();
            data = {'service_delivery_unit': serviceDeliveryUnit};
            if(this.getViewModel().get("theServiceDeliveryUnit").phantom == true){
                url = '/service_delivery_units';
                method = 'POST'
            }
            else{
                url = '/service_delivery_units/'+ serviceDeliveryUnit.id;
                method = 'PUT'
            }
            Ext.Ajax.request({
                method: method,
                url: url,
                headers: { 'Content-Type': 'application/json' },
                jsonData:data,
                success: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    if (result.success) {
                        me.getStore('serviceDeliveryUnits').load();
                        me.onCancelButtonClick();
                        CM.util.Util.showToast(result.message);
                    } else {
                        CM.util.Util.showErrorMsg(result.message);
                    }
                },
                failure: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    CM.util.Util.showErrorMsg(result.message);
                }
            });
        }
    },

    onServicesButtonClick: function(btn){
        "use strict";
        var record = btn.getWidgetRecord(),
            applicableServicesStore,
            chargeableServicesStore,
            serviceDeliveryUnitId = record.get('id'),
            win = Ext.create('PH.view.servicedeliveryunitservice.ServiceDeliveryUnitServiceView', {title: record.get('name') + ' Services'});
        applicableServicesStore = win.getViewModel().get('applicableServices');
        chargeableServicesStore = win.getViewModel().get('chargeableServices');
        applicableServicesStore.load({
            url: '/service_delivery_units/'+serviceDeliveryUnitId+'/chargeable_services'
        });
        chargeableServicesStore.load({
            url: '/service_delivery_units/'+serviceDeliveryUnitId+'/unassigned_services'
        });
        win.lookupReference('service_delivery_unit_id').setValue(serviceDeliveryUnitId);
        win.show();
    },

    onCancelButtonClick: function(){
        "use strict";
        var view = this.getView();
        view.up().up().getEl().unmask();
        this.dialog = Ext.destroy(this.dialog);
    }
});
