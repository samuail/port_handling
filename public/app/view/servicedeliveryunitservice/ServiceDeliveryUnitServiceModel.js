/**
 * Created by betse on 10/20/15.
 */
Ext.define('PH.view.servicedeliveryunitservice.ServiceDeliveryUnitServiceModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.service-delivery-unit-service',
    requires: ['PH.model.chargeableservice.ChargeableService'],

    stores:{
        applicableServices: {
            model: 'PH.model.chargeableservice.ChargeableService',
            autoLoad: false
        },
        chargeableServices:{
            model: 'PH.model.chargeableservice.ChargeableService',
            autoLoad: false
        }
    }
});
