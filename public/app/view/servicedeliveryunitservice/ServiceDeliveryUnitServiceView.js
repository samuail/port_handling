/**
 * Created by betse on 10/20/15.
 */
Ext.define('PH.view.servicedeliveryunitservice.ServiceDeliveryUnitServiceView',{
    extend: 'Ext.window.Window',
    requires: [
        'PH.view.servicedeliveryunitservice.ServiceDeliveryUnitServiceController',
        'PH.view.servicedeliveryunitservice.ServiceDeliveryUnitServiceModel',
        'PH.view.servicedeliveryunitservice.ServiceDeliveryUnitServiceGrid'
    ],
    controller: 'service-delivery-unit-service',
    viewModel:{
        type: 'service-delivery-unit-service'
    },
    session: true,
    width: 800,
    height: 450,
    modal: true,
    items:[
        {
            xtype: 'hiddenfield',
            reference: 'service_delivery_unit_id'
        },
        {
            xtype: 'service-delivery-unit-service-grid',
            flex: 1,
            reference: 'serviceDeliveryUnitServiceGrid'
        }
    ]
});
