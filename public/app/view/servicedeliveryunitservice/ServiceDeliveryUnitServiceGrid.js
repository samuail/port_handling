/**
 * Created by betse on 10/20/15.
 */
Ext.define('PH.view.servicedeliveryunitservice.ServiceDeliveryUnitServiceGrid',{
    extend: 'CM.base.BaseGridWithSave',
    alias: 'widget.service-delivery-unit-service-grid',
    bind: '{applicableServices}',
    height: 400,
    session: true,
    columns:[
        {
            text: 'Chargeable Services',
            dataIndex: 'name',
            flex: 1
        }
    ],
    listeners:{
        afterrender: 'onAfterRender'
    }
});
