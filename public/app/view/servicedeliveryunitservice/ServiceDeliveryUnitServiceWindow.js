/**
 * Created by betse on 10/20/15.
 */
Ext.define('PH.view.servicedeliveryunitservice.ServiceDeliveryUnitServiceWindow',{
    extend: 'CM.base.BaseWindow',
    alias: 'widget.service-delivery-unit-service-window',
    width: 400,
    height: 200,
    bind:'{title}',
    session: true,
    items:[
        {
            xtype: 'hiddenfield',
            reference: 'service_delivery_unit_id_1'
        },
        {
            xtype: 'grid',
            reference: 'serviceDeliveryUnitServiceGridWindow',
            selType: 'checkboxmodel',
            bind: '{chargeableServices}',
            columnLines: true,
            columns: [
                {
                    text: "Name",
                    dataIndex: 'name',
                    flex: 1
                }
            ]
        }
    ],
    listeners:{
        close: 'onCancelButtonClick'
    },
    dockedItems: [
        {xtype: 'savecanceltoolbar'}
    ]
});
