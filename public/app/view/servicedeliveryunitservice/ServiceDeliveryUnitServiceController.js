/**
 * Created by betse on 10/20/15.
 */
Ext.define('PH.view.servicedeliveryunitservice.ServiceDeliveryUnitServiceController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.service-delivery-unit-service',
  requires: [
    'PH.view.servicedeliveryunitservice.ServiceDeliveryUnitServiceWindow',
    'CM.util.Util'
  ],

  createDialog: function (record) {
    var me = this,
        view = me.getView();
    this.dialog = view.add({
      xtype: 'service-delivery-unit-service-window',
      viewModel: {
        data: {
          title: 'Assign service'
        }
      }
    });
    view.mask();
    this.dialog.show();
  },
  onAddButtonClick: function () {
    "use strict";
    this.createDialog(null);
  },

  onCancelButtonClick: function () {
    "use strict";
    var view = this.getView();
    view.getEl().unmask();
    this.dialog = Ext.destroy(this.dialog);
  },

  onSaveButtonClick: function () {
    "use strict";
    var me = this,
        grid = me.lookupReference('serviceDeliveryUnitServiceGridWindow'),
        data,
        serviceDeliveryUnitId = parseInt(this.lookupReference('service_delivery_unit_id').value);
    for (var i = 0; i < grid.getSelectionModel().getSelection().length; i++) {
      data = {
        'service_delivery_unit_service': {
          'service_delivery_unit_id': serviceDeliveryUnitId,
          'chargeable_service_id': grid.getSelectionModel().getSelection()[i].data.id
        }
      };
      Ext.Ajax.request({
        method: 'POST',
        url: '/service_delivery_unit_services',
        headers: {'Content-Type': 'application/json'},
        jsonData: data,
        success: function (response) {
          var result = CM.util.Util.decodeJSON(response.responseText);
          if (result.success) {
            me.getStore('applicableServices').load({
              url: '/service_delivery_units/' + serviceDeliveryUnitId + '/chargeable_services'
            });
            me.getStore('chargeableServices').load({
              url: '/service_delivery_units/' + serviceDeliveryUnitId + '/unassigned_services'
            });
            me.onCancelButtonClick();
            CM.util.Util.showToast(result.message);
          } else {
            CM.util.Util.showErrorMsg(result.message);
          }
        },
        failure: function (response) {
          var result = CM.util.Util.decodeJSON(response.responseText);
          CM.util.Util.showErrorMsg(result.message);
        }
      });
    }
  },
  onAfterRender: function(){
      "use strict";
      Ext.ComponentQuery.query("#save")[0].hide();
  }
});
