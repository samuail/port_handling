/**
 * Created by betse on 10/21/15.
 */
Ext.define('PH.view.breakbulkrate.BreakBulkRateController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.break-bulk-rate',
    requires: [
     'CM.util.Util'
    ],

     onGenerateButtonClick: function(){
         "use strict";
         var me = this,
         effectiveDate;
         effectiveDate = me.lookupReference('effectiveDate').value;
         if(effectiveDate == null)
            CM.util.Util.showErrorMsg("Please enter effective date")
         else{
             Ext.Ajax.request({
                 method: 'POST',
                 url: '/break_bulk_rates',
                 headers: { 'Content-Type': 'application/json' },
                 jsonData:{
                 effective_date : effectiveDate
                 },
                 success: function (response) {
                     var result = CM.util.Util.decodeJSON(response.responseText);
                     if (result.success) {
                         me.getStore('breakBulkRates').load();
                         CM.util.Util.showToast(result.message);
                         me.lookupReference('effectiveDate').setValue("");
                     } else {
                        CM.util.Util.showErrorMsg(result.message);
                     }
                 },
                 failure: function (response) {
                     var result = CM.util.Util.decodeJSON(response.responseText);
                     CM.util.Util.showErrorMsg(result.message);
                 }
             });
         }
     },

     onServiceDeliveryUnitChange: function(combo, newValue, oldValue, eOpts){
         "use strict";
         this.getViewModel().getStore('chargeableServices').load({
            url: '/break_bulk_rates/'+0+'/get_chargeable_service_for_service_delivery_unit/'+ newValue
         });
     },

     onChargeableServiceChange: function(combo, newValue, oldValue, eOpts){
         "use strict";
         var me = this,
         portId;
         portId = me.lookupReference('portsCombo').lastValue;
         this.getViewModel().getStore('breakBulkRates').load({
             url: ' /break_bulk_rates/'+portId+'/get_rate_for_chargeable_service/'+newValue
         });
     },

     onListAllButtonClick: function(){
        "use strict";
        this.getViewModel().getStore('breakBulkRates').load()
     },

     onUpdateButtonClick: function(){
         "use strict";
         var me = this,
         modifiedRecords =[],
         ratesStore,
         effectiveDate;
         effectiveDate = me.lookupReference('effectiveDate').value;
         if(effectiveDate == null)
            CM.util.Util.showErrorMsg("Please enter effective date");
         else {
             ratesStore = me.getViewModel().getStore('breakBulkRates').getModifiedRecords();
             for (var i = 0; i < ratesStore.length; i++) {
                 modifiedRecords.push(ratesStore[i].data);
             }
             if (modifiedRecords.length > 0) {
                 Ext.Ajax.request({
                     method: 'PUT',
                     url: '/break_bulk_rates/' + ratesStore[0].data.id,
                     headers: {'Content-Type': 'application/json'},
                     jsonData: {
                         updated_service_rates: modifiedRecords
                     },
                     success: function (response) {
                         var result = CM.util.Util.decodeJSON(response.responseText);
                         if (result.success) {
                             me.getStore('breakBulkRates').load();
                             CM.util.Util.showToast(result.message);
                         } else {
                             CM.util.Util.showErrorMsg(result.message);
                         }
                     },
                     failure: function (response) {
                         var result = CM.util.Util.decodeJSON(response.responseText);
                         CM.util.Util.showErrorMsg(result.message);
                     }
                 });
             }else{
                CM.util.Util.showInfoMsg('There is no updated rate');
             }
         }
     }
});
