/**
 * Created by betse on 10/21/15.
 */
Ext.define('PH.view.breakbulkrate.BreakBulkRateModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.break-bulk-rate',
    requires:[
        'PH.model.breakbulkrate.BreakBulkRate',
        'PH.model.servicedeliveryunit.ServiceDeliveryUnit',
        'PH.model.chargeableservice.ChargeableService'
    ],

    stores:{
        breakBulkRates:{
            model: 'PH.model.breakbulkrate.BreakBulkRate',
            autoLoad: true
        },
        serviceDeliveryUnits:{
            model: 'PH.model.servicedeliveryunit.ServiceDeliveryUnit',
            autoLoad: true
        },
        chargeableServices:{
            model: 'PH.model.chargeableservice.ChargeableService',
            autoLoad: false
        }
    }
});
