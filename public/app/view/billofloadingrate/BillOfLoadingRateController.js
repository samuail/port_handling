/**
 * Created by betse on 10/18/15.
 */
Ext.define('PH.view.billofloadingrate.BillOfLoadingRateController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.bill-of-loading-rate',
    requires: [
        'CM.util.Util'
    ],

    onGenerateButtonClick: function(){
        "use strict";
        var me = this,
            effectiveDate;
        effectiveDate = me.lookupReference('effectiveDate').value;
        if(effectiveDate == null)
            CM.util.Util.showErrorMsg("Please enter effective date");
        else{
            Ext.Ajax.request({
                method: 'POST',
                url: '/bill_of_loading_rates',
                headers: { 'Content-Type': 'application/json' },
                jsonData:{
                    effective_date : effectiveDate
                },
                success: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    if (result.success) {
                        me.getStore('billOfLoadingRates').load();
                        CM.util.Util.showToast(result.message);
                        me.lookupReference('effectiveDate').setValue("");
                    } else {
                        CM.util.Util.showErrorMsg(result.message);
                    }
                },
                failure: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    CM.util.Util.showErrorMsg(result.message);
                }
            });
        }
    },

    onServiceDeliveryUnitChange: function(combo, newValue, oldValue, eOpts){
        "use strict";
        this.getViewModel().getStore('chargeableServices').load({
            url: '/bill_of_loading_rates/'+0+'/get_chargeable_service_for_service_delivery_unit/'+newValue
        });
    },

    onChargeableServiceChange: function(combo, newValue, oldValue, eOpts){
        "use strict";
        var me = this,
            serviceDeliveryUnitId;
        serviceDeliveryUnitId = me.lookupReference('portsCombo').lastValue;
        this.getViewModel().getStore('billOfLoadingRates').load({
            url: '/bill_of_loading_rates/'+serviceDeliveryUnitId+'/get_rate_for_chargeable_service/'+newValue
        });
    },

    onListAllButtonClick: function(){
        "use strict";
        this.getViewModel().getStore('billOfLoadingRates').load()
    },

    onUpdateButtonClick: function(){
        "use strict";
        var me = this,
            modifiedRecords =[],
            ratesStore,
            effectiveDate;
        effectiveDate = me.lookupReference('effectiveDate').value;
        if(effectiveDate == null)
            CM.util.Util.showErrorMsg("Please enter effective date");
        else {
            ratesStore = me.getViewModel().getStore('billOfLoadingRates').getModifiedRecords();
            for (var i = 0; i < ratesStore.length; i++) {
                modifiedRecords.push(ratesStore[i].data);
            }
            if(modifiedRecords.length > 0) {
                Ext.Ajax.request({
                    method: 'PUT',
                    url: '/bill_of_loading_rates/' + ratesStore[0].data.id,
                    headers: {'Content-Type': 'application/json'},
                    jsonData: {
                        updated_service_rates: modifiedRecords
                    },
                    success: function (response) {
                        var result = CM.util.Util.decodeJSON(response.responseText);
                        if (result.success) {
                            me.getStore('billOfLoadingRates').load();
                            CM.util.Util.showToast(result.message);
                        } else {
                            CM.util.Util.showErrorMsg(result.message);
                        }
                    },
                    failure: function (response) {
                        var result = CM.util.Util.decodeJSON(response.responseText);
                        CM.util.Util.showErrorMsg(result.message);
                    }
                });
            }else{
                CM.util.Util.showInfoMsg('There is no updated rate');
            }
        }
    }
});
