/**
 * Created by betse on 10/18/15.
 */
Ext.define('PH.view.billofloadingrate.BillOfLoadingRateModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.bill-of-loading-rate',
    requires: [
        'PH.model.billofloadingrate.BillOfLoadingRate',
        'PH.model.servicedeliveryunit.ServiceDeliveryUnit',
        'PH.model.chargeableservice.ChargeableService'
    ],

    stores:{
        billOfLoadingRates:{
            model: 'PH.model.billofloadingrate.BillOfLoadingRate',
            autoLoad: true
        },

        serviceDeliveryUnits:{
            model: 'PH.model.servicedeliveryunit.ServiceDeliveryUnit',
            autoLoad: true
        },

        chargeableServices:{
            model: 'PH.model.chargeableservice.ChargeableService',
            autoLoad: false
        }
    }
});