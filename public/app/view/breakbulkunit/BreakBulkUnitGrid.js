/**
 * Created by betse on 12/11/15.
 */
Ext.define('PH.view.breakbulkunit.BreakBulkUnitGrid',{
    extend: 'CM.base.BaseGrid',
    alias: 'widget.break-bulk-unit-grid',
    requires: [
        'PH.view.breakbulkunit.BreakBulkUnitModel',
        'PH.view.breakbulkunit.BreakBulkUnitController'
    ],
    controller: 'break-bulk-unit',
    viewModel:{
        type: 'break-bulk-unit'
    },
    session: true,
    bind: '{breakBulkUnits}',
    columns:[
        {
            text: 'Code',
            dataIndex: 'code',
            flex: 1
        },
        {
            text: 'Name',
            dataIndex: 'name',
            flex: 2
        }
    ]
});
