/**
 * Created by betse on 12/11/15.
 */
Ext.define('PH.view.breakbulkunit.BreakBulkUnitController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.break-bulk-unit',
    requires:[
        'PH.view.breakbulkunit.BreakBulkUnitWindow',
        'CM.util.Util'
    ],

    createDialog: function(record){
        var me = this,
            view = me.getView();
        this.dialog = view.add({
            xtype: 'break-bulk-unit-window',
            viewModel:{
                data: {
                    title: record ? 'Edit '+ record.get('name') : 'Add break bulk unit'
                },
                links:{
                    theBreakBulkUnit: record||{
                        type: 'PH.model.lookup.LookUp',
                        create: true
                    }
                }
            }
        })
        view.up().up().mask();
        this.dialog.show();
    },
    onAddButtonClick: function(){
        "use strict";
        this.createDialog(null);
    },
    onEditButtonClick: function () {
        "use strict";
        var me = this,
            grid = me.getView(),
            selection = grid.getSelectionModel().getSelection();

        if (selection && selection.length > 0) {
            if (selection.length > 1) {
                CM.util.Util.showInfoMsg('Please select only one item to edit');
            } else {
                this.createDialog(selection[0]);
            }
        } else {
            CM.util.Util.showInfoMsg('You have to select a record to edit!');
        }
    },

    onSaveButtonClick: function () {
        "use strict";
        var me = this,
            form = me.lookupReference('breakBulkUnitForm'),
            url,
            method,
            breakBulkUnit,
            data;
        if(form.isValid())
        {
            breakBulkUnit = this.getViewModel().get("theBreakBulkUnit").getData();
            data = {'break_bulk_unit': breakBulkUnit};
            if(this.getViewModel().get("theBreakBulkUnit").phantom == true){
                url = '/break_bulk_units';
                method = 'POST'
            }
            else{
                url = '/break_bulk_units/'+ breakBulkUnit.id;
                method = 'PUT'
            }
            Ext.Ajax.request({
                method: method,
                url: url,
                headers: { 'Content-Type': 'application/json' },
                jsonData:data,
                success: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    if (result.success) {
                        me.getStore('breakBulkUnits').load();
                        me.onCancelButtonClick();
                        CM.util.Util.showToast(result.message);
                    } else {
                        CM.util.Util.showErrorMsg(result.message);
                    }
                },
                failure: function (response) {
                    var result = CM.util.Util.decodeJSON(response.responseText);
                    CM.util.Util.showErrorMsg(result.message);
                }
            });
        }
    },

    onCancelButtonClick: function(){
        "use strict";
        var view = this.getView();
        view.up().up().getEl().unmask();
        this.dialog = Ext.destroy(this.dialog);
    }
});
