/**
 * Created by betse on 12/11/15.
 */
Ext.define('PH.view.breakbulkunit.BreakBulkUnitModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.break-bulk-unit',
    requires: [
        'PH.model.lookup.LookUp'
    ],

    stores:{
        breakBulkUnits:{
            model: 'PH.model.lookup.LookUp',
            autoLoad: true,
            session: true,
            proxy:{
                type: 'baseproxy',
                url: '/break_bulk_units'
            }
        }
    }
});
