/**
 * Created by betse on 12/11/15.
 */
Ext.define('PH.view.breakbulkunit.BreakBulkUnitWindow', {
  extend: 'CM.base.BaseWindow',
  requires: [
    'CM.base.SaveCancelToolbar'
  ],
  alias: 'widget.break-bulk-unit-window',
  width: 400,
  height: 250,
  bind: '{title}',
  session: true,
  items: [
    {
      xtype: 'form',
      reference: 'breakBulkUnitForm',
      frame: false,
      bodyPadding: 15,
      items: [
        {
          xtype: 'fieldset',
          title: 'Break Bulk Unit Detail',
          layout: 'vbox',
          fieldDefaults: {
            labelAlign: 'top',
            labelWidth: 100,
            labelStyle: 'font-weight:bold',
            margin: '5 8 5 5',
            width: '100%'
          },
          items: [
            {
              xtype: 'textfield',
              fieldLabel: 'Code',
              name: 'code',
              allowBlank: false,
              bind: '{theBreakBulkUnit.code}'
            },
            {
              xtype: 'textfield',
              fieldLabel: 'Name',
              name: 'name',
              allowBlank: false,
              bind: '{theBreakBulkUnit.name}'
            }
          ]
        }
      ]
    }
  ],
  listeners: {
    close: 'onCancelButtonClick'
  },
  dockedItems: [
    {xtype: 'savecanceltoolbar'}
  ]
});
