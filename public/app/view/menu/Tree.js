/**
 * Created by betse on 5/16/2015.
 */
Ext.define('PH.view.menu.Tree',{
    extend: 'Ext.tree.Panel',
    xtype: 'menu-tree',
    border: 0,
    autoScroll: true,
    rootVisible: false
});
