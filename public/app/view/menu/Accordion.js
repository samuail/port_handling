/**
 * Created by betse on 5/16/2015.
 */
Ext.define('PH.view.menu.Accordion',{
    extend: 'Ext.panel.Panel',
    xtype: 'main-menu',
    width: '250',
    layout: {
        type: 'accordion',
        multi: true
    },
    collapsible: true,
    split: true,
    iconCls: 'fa fa-sitemap fa-lg',
    title: 'Menu'
});
