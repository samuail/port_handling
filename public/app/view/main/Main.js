/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('PH.view.main.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'PH.view.main.MainController',
        'PH.view.main.MainModel',
        'PH.view.main.MainPanel',
        'PH.view.menu.Accordion',
        'PH.view.main.Header'
    ],
    plugins: 'viewport',
    xtype: 'app-main',

    controller: 'main',
    viewModel: {
        type: 'main'
    },

    layout: {
        type: 'border'
    },

    items: [
        {
            xtype: 'appheader',
            region: 'north'
        },
        {
            region: 'west',
            xtype: 'main-menu',
            width: 250
        },
        {
            region: 'center',
            xtype: 'mainpanel'
        },
        {
            xtype: 'container',
            region: 'south',
            height: 30,
            style: 'border-top: 1px solid #4c72a4;',
            html: '<div style="text-align: center"><span style="font-size:10px; margin: 0 auto;">Quotation Manager : Developed By Mentor Knowledge Solutions (MKS)</span></center></div>'
        }
    /*{
        xtype: 'panel',
        bind: {
            title: '{name}'
        },
        region: 'west',
        html: '<ul><li>This area is commonly used for navigation, for example, using a "tree" component.</li></ul>',
        width: 250,
        split: true,
        tbar: [{
            text: 'Button',
            handler: 'onClickButton'
        }]
    },{
        region: 'center',
        xtype: 'tabpanel',
        *//*items:[{
            *//**//*title: 'Chargeable Services',
            xtype: 'chargeable-service-grid'
            title: 'Increments',
            xtype: 'increment-grid'*//**//*
            title: 'Bill of Loading',
            xtype: 'bill-of-loading-grid'
            *//**//*title: 'Quotation Items',
            xtype: 'quotation-item-grid'
            title: 'Generate Rate',
            xtype: 'chargeable-service-rate-grid'
            title: 'Bill of Loading Rate',
            xtype: 'bill-of-loading-rate-grid'
            title: 'Service Delivery Units',
            xtype: 'service-delivery-unit-grid'
            title: 'Break Bulk rates',
            xtype: 'break-bulk-rate-grid'*//**//*
        }]*//*
    }*/]
});
