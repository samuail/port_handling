/**
 * Overview:
 * Author: Henock L. (henockl@live.com)
 * Date: 4/20/2014
 * Time: 9:20 AM
 */
Ext.define('PH.view.main.Header', {
  extend: 'Ext.toolbar.Toolbar',
  alias: 'widget.appheader',
  height: 30,
  ui: 'footer',
  style: 'border-bottom: 4px solid #4c72a4',
  items: [
    {
      xtype: 'label',
      html: '<div id="titleHeader"><b>Quotation Manager</b><span style="font-size: 12px;"></span></div>'
    },
    {
      xtype: 'tbfill'
    },
    {
      xtype: 'label',
      itemId: 'userLabel'
    },
    {
      xtype: 'tbseparator'
    },
    {
      xtype: 'button',
      itemId: 'logout',
      reference: 'logout',
      text: 'Logout',
      //glyph: 'xf011@FontAwesome',
      iconCls: 'fa fa-power-off fa-lg logoutIcon',
      handler: 'onLogoutButtonClick'
    }
  ]
});