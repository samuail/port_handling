/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('PH.view.main.MainController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.main',
  onLogoutButtonClick: function (button) {
    "use strict";
    Ext.Ajax.request({
      url: '/auth/logout',
      success: function (conn) {
        var result = CM.util.Util.decodeJSON(conn.responseText);

        if (result.success) {
          button.up('app-main').destroy();
          window.location.href = '/';
        } else {
          PH.util.Util.showErrorMsg(result.message);
        }
      },
      failure: function (conn) {
        PH.util.Util.showErrorMsg(conn.responseText);
      }
    });
  }
});
