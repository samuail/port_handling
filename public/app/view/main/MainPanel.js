/**
 * Created by betse on 5/4/2015.
 */
Ext.define('PH.view.main.MainPanel', {
  extend: 'Ext.tab.Panel',
  xtype: 'mainpanel',
  activeTab: 0,
  items: [
    {
      xtype: 'panel',
      closeable: false,
      iconCls: 'fa fa-home fa-lg tabIcon',
      title: 'Home',
      items: [
        {
          xtype: 'panel',
          html: '<h2>Welcome!</h2> <br>Please select an item from the menu on the left side to carry out a task.'
        }
      ]
    }
  ]
});
